angular.module('app').factory('$dataServices', function($http) {


    var $dataServices = [];

    $dataServices.getParticipants = function() {

        var participants  = $http.get('json/participants.json');

        return participants;
    }


    $dataServices.listenForChange = function(i) {

        console.log(i);
    }

    return $dataServices;

});