angular.module('app').directive('navigation', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/navbar.html',
        replace:false
    }
});

angular.module('app').directive('loggedinNavigation', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/loggedin-navbar.html',
        replace:false
    }
});

angular.module('app').directive('login', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/login.html',
        replace:true,
        controller:'ContentCtrl'
    }
});

angular.module('app').directive('registration', function() {
    return {
        restriction:'E',
        templateUrl:'ng-templates/registration.php',
        replace:false,
        controller:'ContentCtrl'
    }
});

angular.module('app').directive('account', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/account.php',
        replace:false,
        controller:'ContentCtrl'
    }
});

angular.module('app').directive('insights', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/insights.php',
        replace:false
    }
});

angular.module('app').directive('about', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/about.html',
        replace:false
    }
});

angular.module('app').directive('snapshot', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/snapshot.php',
        replace:false,
        controller:'ContentCtrl'
    }
});



angular.module('app').directive('cases', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/cases.php',
        replace:false,
        controller:'ContentCtrl'
    }
});

angular.module('app').directive('case', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/case.php',
        replace:false,
    }
});

angular.module('app').directive('barometer', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/barometer.php',
        replace:false,
        controller:'ContentCtrl'
    }
});

angular.module('app').directive('bestPractices', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/best-practices.php',
        replace:false,
        controller:'ContentCtrl'
    }
});

angular.module('app').directive('adminNav', function() {

    return {
        restriction:'E',
        templateUrl:'ng-templates/admin-navbar.html',
        replace:false,
        controller:'ContentCtrl'
    }
});