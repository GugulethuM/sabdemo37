$(document).on('ready', function () {

    TweenMax.set('#main', { xPercent: 100 });
    
    

});
/**
 * 
 * @param {*} str 
 */
function trim(str) {
    return str.replace("/^\s+|\s+$/", "");
}

/**
 * @description Generate hover states for side items - parent and child level
 * @author J.Brewis
 * @returns void
 */
function sidebarHovers() {

    // remove menu if desktop
    if ($(window).width() > 768) {
        
        $('.hide-desktop').remove();
    }


    /* DROPDOWN MOUSE OVER AND LEAVE 
    *------------------------------*/
    $('.dropdown > ul > li').on('mouseover', function () {

        TweenMax.to($(this), 0.5, { color: '#5bbec8', fontWeight: 'bold', ease: Power4.easeOut });

    });

    $('.dropdown > ul > li').on('mouseleave', function () {

        TweenMax.to($(this), 0.5, { color: '#ffffff', fontWeight: 'normal', scale: 1, ease: Power4.easeOut });

    });

    /* HOVER AND CLICK STATES FOR SIDE BAR ITEMS
    *--------------------------------------------
    */
    // mouse over
    $('.homepage__sidenav > ul > li').on('mouseover', function () {

        var tl = new TimelineMax(),
            mainLi = $(this).find('.main-li');


        if (!$(this).hasClass('active')) {

            tl.to(mainLi, 0.5, { backgroundColor: '#5bbec8', ease: Expo.easeOut }, 0)
                .to($(this).find('.dropdown--line'), 0.5, { width: '89%', autoAlpha: 1, display: 'block', ease: Expo.easeOut }, 0)

                ;
        }
    });

    // mouseleave
    $('.homepage__sidenav > ul > li').on('mouseleave', function () {


        if (!$(this).hasClass('active')) {

            var tl = new TimelineMax(),
                mainLi = $(this).find('.main-li');

            tl
                .to($(this).find('.dropdown--line'), 0.5, { width: '0%', opacity: 0, ease: Expo.easeOut }, 0)
                .to($(this).find('.main-li'), 0.5, { backgroundColor: '#363636', ease: Expo.easeOut }, 0)

                ;
        }
    });

    //click
    $('.homepage__sidenav > ul > li').on('click', function () {


        var tl = new TimelineMax();

        tId = $(this).attr('data-target'),
            tDiv = $("#dd" + tId),
            paddingBottom = 20,
            liDiv = $('#li' + tId);
        arrow = $(this).find('.dropdown--arrow'),
            mainLi = $(this).find('.main-li')
            ;
        tDiv.addClass('active');
        liDiv.addClass('active');


        $('.homepage__sidenav li').each(function () {


            if (tId != $(this).attr('data-target')) {
                TweenMax.to($(this).find('.main-li'), 0.5, { backgroundColor: '#363636', ease: Expo.easeOut }, 0);
                TweenMax.to($(this).find('.dropdown--line'), 0.5, { width: '0%', autoAlpha: 0, display: 'none', ease: Expo.easeOut }, 0);

                $(this).removeClass('active');
            }


        });

        $('.dropdown').each(function () {

            var id = $(this).attr('id');
            var onlyNumber = id.replace(/[a-z]+/, '');
            var parentLi = $("#li" + onlyNumber);


            if (tId != onlyNumber) {

                $(this).removeClass('active');
                parentLi.removeClass('active');

                // remove li animation
                var tl = new TimelineMax();

                tl
                    .to(parentLi.find('.dropdown--line'), 0.5, { width: '0%', opacity: 0, ease: Expo.easeOut }, 0)
                    .to(parentLi.find('.main-li'), 0.5, { backgroundColor: '#363636', ease: Expo.easeOut }, 0)
                //
                TweenMax.to($(this), 0.5, { height: 0, ease: Power4.easeOut });
                TweenMax.to(parentLi.find('.dropdown--arrow'), 0.5, { rotation: 0, ease: Power4.easeOut });
            }

        });

        //
        var dropDownHeight = tDiv.children(0).children(0).height() * tDiv.children(0).children().length + paddingBottom;


        if (tDiv.height() == 0) {

            tl.to(tDiv, 0.6, { height: dropDownHeight, maxHeight: 'inherit', ease: Expo.easeIn })
                .to(arrow, 0.5, { rotation: 90 }, 0)
                ;
        }
        else {
            tl.to(tDiv, 0.6, { height: 0, ease: Expo.easeIn }, 0)
                .to(arrow, 0.5, { rotation: 0 }, 0)
                ;
        }



    });


}


function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) { return pair[1]; }
    }
    return (false);
}