angular.module('app').controller('SidebarCtrl', ['$scope','$dataServices','$controller', function($scope, $dataServices, $controller) {


    $scope.dataServices = [];
    $scope.categories = {};

    var vm = this;

    angular.extend(vm, $controller['ContentCtrl'],{$scope:$scope});

    $scope.init = function() {

        // get data to create directories
        $dataServices.getParticipants().then(function(data) {


            for (var i=0; i < data.data.participants.length; i++) 
            {
               
                    var cat = {
                        "category":data.data.participants[i].category,
                        "subCategory":data.data.participants[i].subCategory,
                        "id":data.data.participants[i].id,
                        "cid":data.data.participants[i].cid,
                        "sCid":data.data.participants[i].sCidId
                    }
                    if ($scope.categories['cid'+cat.cid] == undefined) {
                        $scope.categories['cid'+cat.cid] = {'categoryName':cat.category,'id':cat.id, 'cid':cat.cid, 'scid':cat.sCid};
                    }
                    

                    if ($scope.categories['cid'+cat.cid].children == undefined) {

                        $scope.categories['cid'+cat.cid].children = [];
                    }

                    if (cat.subCategory != "") {
                       
                        $scope.categories['cid'+cat.cid].children.push({'name':cat.subCategory,'sid':cat.sCid});
                    }
            }

        });
    }


    $scope.getCategories = function(item) {

        // close mobi panel if mobi

        if (parseInt($(window).width()) >= 320 && parseInt($(window).width()) <= 768) {

            $('.navbar-toggle').trigger( "click" );
                    
        }
        ;
        
        $scope.filterResults(item);
        
        $scope.category = item.categoryName;
        $scope.subCategory = '';
        //$rootScope.categoryChanged = item;
        // console.log( $rootScope.categoryChanged);
        $scope.goToCategories();

        
        
    }

    $scope.getSubCategories = function(category, subCategory) {

        if (parseInt($(window).width()) >= 320 && parseInt($(window).width()) <= 768) {

            $('.navbar-toggle').trigger( "click" );
                    
        }
        ;

        
        $scope.category = category.categoryName;
        $scope.subCategory = subCategory.name;

        // console.log(category, subCategory);

        $scope.filterSubcategory(category, subCategory);


    }
        
    

}]);