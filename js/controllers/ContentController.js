angular.module('app').controller('ContentCtrl', ['$scope','$sce','$location', function($scope, $dataServices, $sce, $location) {

    $scope.activeSet = 0;

    $scope.to_trusted = function(html_code) {
        return $sce.trustAsHtml(html_code);
    }

    $scope.setActive = function(id) {

       
        $scope.activeSet = id;
       
    }

    $scope.linkToSection = function(section) {

        console.log(section);
    }

    $scope.calculateHeightLogin = function() {}


    $scope.formatFilter = function(id) {
        $scope.setActive(id);
        var btn = $('#snapshotfilter'), dropdown = $('#snapshot_group');
        
        btn.on('click', function() {
            var h = parseInt(dropdown.height());

            var c = $('.filter-group').length;
            var itemH = $('.filter-group').height();
            var arrow = $('.btn-down');
            var padding = 30;

            console.log(c, itemH);
            //var hAni = c * itemH + padding;
            var hAni = document.body.offsetHeight - btn.offset().top - 100; // Height of the page  - button top offset
            console.log(hAni);

            if(h <= 20){
                    TweenMax.to(dropdown, 0.5, {height: hAni, ease:Expo.easeInOut, marginTop:document.querySelector(".btn__snapshot").offsetHeight});
                    TweenMax.to(arrow, 0.5, {rotation: 90, ease:Expo.easeInOut});
            } else{
                    TweenMax.to(dropdown, 0.8, {height: 0, ease:Expo.easeInOut, marginTop:0});
                    TweenMax.to(arrow, 0.5, {rotation: 0, ease:Expo.easeInOut});
            }
        });
    }

    $scope.registerForm = function(){
        if(document.querySelector("form[name='registrationform']")){
            var regform = document.querySelector("form[name='registrationform']");
            regform.addEventListener("submit", function(e){
                this.regsubmit.setAttribute("disabled", "disabled");
                var errors = false;
                // Check if password match
                if(this.password.value != this.password2.value){
                    this.password2.setCustomValidity("Passwords do not match");
                    errors = true;
                } else
                    this.password2.setCustomValidity("");
                if(!errors){
                    var regdata = "";
                    for(var re = 0; re < this.elements.length; ++re){
                        if(this.elements[re].type != "submit")
                            regdata += (regdata.length > 0) ? "&" + this.elements[re].name + "=" + this.elements[re].value : this.elements[re].name + "=" + this.elements[re].value;
                        else
                            continue;
                    }
                    // create the AJAX object
                    var regxhr = new XMLHttpRequest();
                    regxhr.open("POST", this.action, true);
                    regxhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    //xhr.setRequestHeader("Content-Type", "multipart/form-data");
                    regxhr.responseType = "json";
                    regxhr.onload = function(e){
                        if(this.status == 200){
							var registerresponse = (typeof this.response === "string") ? JSON.parse(this.response) : this.response;
                            if("success" in registerresponse){
                                window.location.href = "./?registration=success";
                            } else if("errors" in registerresponse){
                                document.querySelector("form[name='registrationform']").regsubmit.removeAttribute("disabled");
                                var errormessage = "";
                                for(var em = 0; em < registerresponse.errors.length; ++em){
                                    errormessage = errormessage + ((errormessage.length > 0) ? "\n" + registerresponse.errors[em] : registerresponse.errors[em]);
                                }
                                alert(errormessage);
                                /*var errormessage = document.querySelector("#errorcontainer");
                                if(errormessage){
                                    // Clean out the messages
                                    if(errormessage.hasChildNodes()){
                                        do{
                                            errormessage.removeChild(errormessage.childNodes[0]);
                                        } while(errormessage.hasChildNodes());
                                    }
                                    for(var em = 0; em < this.response.error.length; ++em){
                                        if(em > 0)
                                            errormessage.appendChild(document.createElement("BR"));
                                        errormessage.appendChild(createErrorSpan("submit", this.response.error[em]));
                                    }
                                    setTimeout(function(){
                                        do{
                                            errormessage.removeChild(errormessage.childNodes[0]);
                                        } while(errormessage.hasChildNodes());
                                    }, 5000);
                                }*/
                                // Loop through each of the errors - add the custom error states for each of the offending fields
                            }
                        }
                    };
                    regxhr.send(regdata);
                } else
                    this.regsubmit.removeAttribute("disabled");
                e.preventDefault();
                return false;
            }, true);

            regform.phone.addEventListener("keyup", function(e){
                this.value = this.value.replace(/\s+/g, "");
            }, false)
        }
    }

    $scope.updateForm = function(){
        if(document.querySelector("form[name='accountform']")){
            var updateform = document.querySelector("form[name='accountform']");
            updateform.addEventListener("submit", function(e){
                this.updatesubmit.setAttribute("disabled", "disabled");
                var errors = false;
                if(!errors){
                    var updatedata = "";
                    for(var ue = 0; ue < this.elements.length; ++ue){
                        if(this.elements[ue].type != "submit")
                            updatedata += (updatedata.length > 0) ? "&" + this.elements[ue].name + "=" + this.elements[ue].value : this.elements[ue].name + "=" + this.elements[ue].value;
                        else
                            continue;
                    }
                    // create the AJAX object
                    var updatexhr = new XMLHttpRequest();
                    updatexhr.open("POST", this.action, true);
                    updatexhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    //xhr.setRequestHeader("Content-Type", "multipart/form-data");
                    updatexhr.responseType = "json";
                    updatexhr.onload = function(e){
                        if(this.status == 200){
							var updateresponse = (typeof this.response === "string") ? JSON.parse(this.response) : this.response;
                            if("success" in updateresponse){
                                window.location.href = "./indsight";
                            } else if("errors" in updateresponse){
                                document.querySelector("form[name='accountform']").updatesubmit.removeAttribute("disabled");
                                /*var errormessage = document.querySelector("#errorcontainer");
                                if(errormessage){
                                    // Clean out the messages
                                    if(errormessage.hasChildNodes()){
                                        do{
                                            errormessage.removeChild(errormessage.childNodes[0]);
                                        } while(errormessage.hasChildNodes());
                                    }
                                    for(var em = 0; em < this.response.error.length; ++em){
                                        if(em > 0)
                                            errormessage.appendChild(document.createElement("BR"));
                                        errormessage.appendChild(createErrorSpan("submit", this.response.error[em]));
                                    }
                                    setTimeout(function(){
                                        do{
                                            errormessage.removeChild(errormessage.childNodes[0]);
                                        } while(errormessage.hasChildNodes());
                                    }, 5000);
                                }*/
                                // Loop through each of the errors - add the custom error states for each of the offending fields
                            }
                        }
                    };
                    updatexhr.send(updatedata);
                } else
                    this.updatesubmit.removeAttribute("disabled");
                e.preventDefault();
                return false;
            }, true);

            updateform.phone.addEventListener("keyup", function(e){
                this.value = this.value.replace(/\s+/g, "");
            }, false)
        }
    }

    $scope.loginForm = function(){

        var regparam = window.location.search.substring(1);
        var regparams = regparam.split("&");
        if(regparams != ""){
            for(var r = 0; r < regparams.length; ++r){
                var rp = regparams[r].split("=");
                if(rp[0] == "registration" && rp[1] == "success")
                    $("#regmodal").modal();
                if(rp[0] == "verified" && rp[1] == "true")
                    $("#verifymodal").modal();
                if(rp[0] == "verified" && rp[1] == "false")
                    $("#verifymodalfalse").modal();
            }
        }

        if(document.querySelector("form[name='loginform']")){
            var loginform = document.querySelector("form[name='loginform']");
            loginform.addEventListener("submit", function(e){
                this.loginsubmit.setAttribute("disabled", "disabled");
                //return true;



                var logindata = "";
                for(var le = 0; le < this.elements.length; ++le){
                    if(this.elements[le].type != "submit")
                        logindata += (logindata.length > 0) ? "&" + this.elements[le].name + "=" + this.elements[le].value : this.elements[le].name + "=" + this.elements[le].value;
                    else
                        continue;
                }
                // create the AJAX object
                var loginxhr = new XMLHttpRequest();
                loginxhr.open("POST", this.action, true);
                loginxhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                //xhr.setRequestHeader("Content-Type", "multipart/form-data");
                loginxhr.responseType = "json";
                loginxhr.onload = function(e){
                    if(this.status == 200){
						var loginresponse = (typeof this.response === "string") ? JSON.parse(this.response) : this.response;
                        if("success" in loginresponse){
                            window.location = "./indsight";
                            //alert("reloading");
                            //window.location.reload();
                        } else if("errors" in loginresponse){
                            var errormsg = "";
                            for(var e = 0; e < loginresponse.errors.length; ++e){
                                errormsg += loginresponse.errors[e];
                                if(e < loginresponse.errors.length - 1)
                                    errormsg += "\n";
                            }
                            alert(errormsg);
                            document.querySelector("form[name='loginform']").reset();
                            document.querySelector("form[name='loginform']").loginsubmit.removeAttribute("disabled");
                            return;
                            /*var errormessage = document.querySelector("#errorcontainer");
                            if(errormessage){
                                // Clean out the messages
                                if(errormessage.hasChildNodes()){
                                    do{
                                        errormessage.removeChild(errormessage.childNodes[0]);
                                    } while(errormessage.hasChildNodes());
                                }
                                for(var em = 0; em < this.response.error.length; ++em){
                                    if(em > 0)
                                        errormessage.appendChild(document.createElement("BR"));
                                    errormessage.appendChild(createErrorSpan("submit", this.response.error[em]));
                                }
                                setTimeout(function(){
                                    do{
                                        errormessage.removeChild(errormessage.childNodes[0]);
                                    } while(errormessage.hasChildNodes());
                                }, 5000);
                            }*/
                            // Loop through each of the errors - add the custom error states for each of the offending fields
                        }
                    }
                };
                loginxhr.send(logindata);
                
                e.preventDefault();
                return false;
            }, true);
        }
    }

    $scope.setupExtract = function(){
        var extractbutton = document.querySelector("button#extractreport");
        extractbutton.addEventListener("click", function(e){
            e.preventDefault();
            window.print();
        }, true);
    }

    $scope.initBCSearch = function(){
        $("#search").autocomplete({
            source: "utility/bcsearch.php",
            minLength: 2,
            select: function(event, ui){
                //console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );
                window.location.href = "./business-case/" + ui.item.id;
            }
        });
    }

    $scope.initContentBlocks = function(){
        var contentitems = document.querySelectorAll(".snapshot__item,.cases__item"); //,.barometer__item
        if(contentitems.length > 0){
            var maxh = 0;
            for(var ci = 0; ci < contentitems.length; ++ci){
                if(contentitems[ci].offsetHeight > maxh)
                    maxh = contentitems[ci].offsetHeight;
            }
            for(var cir = 0; cir < contentitems.length; ++cir){
                contentitems[cir].style.height = maxh + "px";
            }
        }
    }

}])