<?php
    session_name("INDSights");
    session_start();

    if(!isset($_SESSION["loggedin"]) || !$_SESSION["loggedin"])
        header("Location: /");
    
    $businesscaseid = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);
    $_SESSION["bcid"] = $businesscaseid;
?><!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INDSights</title>
    <base href="/staging.dev/indsights/" />
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="js/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
     <!-- Global site tag (gtag.js) - Google Analytics -->
     <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145584979-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    
    gtag('config', 'UA-145584979-1');
    </script>
</head>
<body>
   
    <loggedin-navigation></loggedin-navigation>
    <admin-nav ng-init="setActive(3)"></admin-nav>
    <case></case>

    <script src="js/jquery.min.js"></script>
    <script src="js/angular.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/gsap/TweenMax.js"></script>
    <script src="js/gsap/src/minified/plugins/CSSPlugin.min.js"></script>
    <script src="js/gsap/ScrollToPlugin.js"></script>
    <script src="js/main-ng.js"></script>
    <script src="js/ng-directives.js"></script>
    <script src="js/controllers/ContentController.js"></script>
</body>
</html>