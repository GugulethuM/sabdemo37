<?php
	//stripslashes
	$industry = filter_input(INPUT_POST, "industry", FILTER_SANITIZE_NUMBER_INT);
	$body = htmlentities(strip_tags($_POST["body"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	//$body = filter_var($body, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_ENCODE_HIGH|FILTER_FLAG_ENCODE_AMP);
	if(!!$industry && !!$body){
		require_once("../utility/config/database.php");
		$indsightdata = array($body, $industry);
		$db = new dataHandler();
		$indsightid = $db->addIndustryIndsight($indsightdata);
	}
	header("Location: indsights.php");