<?php
	//stripslashes
	$industry = filter_input(INPUT_POST, "industry", FILTER_SANITIZE_NUMBER_INT);
	$body = htmlentities(strip_tags($_POST["body"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	if(!!$industry && !!$body){
		require_once("../utility/config/database.php");
        $indsightdata = array($body, $industry);
		$db = new dataHandler();
		$db->updateIndustryIndsight($indsightdata);
	}
	header("Location: indsights.php");