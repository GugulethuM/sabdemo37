<?php
	session_name("INDSightsAdmin");
    session_start();
	if((!isset($_POST["username"]) || empty($_POST["username"])) || (!isset($_POST["password"]) || empty($_POST["password"]))){
		$_SESSION["error"] = "<font color=red>Invalid login.</font>";
		header("Location: login.php");
	} else{
		$username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH|FILTER_FLAG_STRIP_BACKTICK);
		$password = filter_input(INPUT_POST, "password", FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH|FILTER_FLAG_STRIP_BACKTICK);
		if(!!$username && !!$password){
			require_once("../utility/config/database.php");
			$lidb = new dataHandler();
			$adminuser = $lidb->loginAdmin($username, $password);
			if(!$adminuser){
				$_SESSION["error"] = "<font color=red>Wrong username or password. Try again.</font>";
				header("Location: login.php");
			} else{
				$_SESSION["loggedin"] = TRUE;
				header("Location: index.php");
				exit;
			}
		} else{
			$_SESSION["error"] = "<font color=red>Invalid login.</font>";
			header("Location: login.php");
		}
	}