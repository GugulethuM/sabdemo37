<?php
	//stripslashes
	$industry = filter_input(INPUT_POST, "industry", FILTER_SANITIZE_NUMBER_INT);
	$political = htmlentities(strip_tags($_POST["political"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$economical = htmlentities(strip_tags($_POST["economical"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$social = htmlentities(strip_tags($_POST["social"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$technological = htmlentities(strip_tags($_POST["technological"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$legal = htmlentities(strip_tags($_POST["legal"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$environmental = htmlentities(strip_tags($_POST["environmental"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	if(!!$industry && !!$political && !!$economical && !!$social && !!$technological && !!$legal && !!$environmental){
		require_once("../utility/config/database.php");
        $snapshotdata = array($political, $economical, $social, $technological, $legal, $environmental, $industry);
		$db = new dataHandler();
		$db->updateIndustrySnapshot($snapshotdata);
	}
	header("Location: snapshots.php");