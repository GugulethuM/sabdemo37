<?php
	//stripslashes
	$industry = filter_input(INPUT_POST, "industry", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_ENCODE_HIGH|FILTER_FLAG_ENCODE_AMP);
	if(!!$industry){
		require_once("../utility/config/database.php");
		$industrydata = array($industry);
		$db = new dataHandler();
		$industryid = $db->addIndustry($industrydata);
	}
	header("Location: industries.php");