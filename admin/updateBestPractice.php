<?php
	//stripslashes
	$industry = filter_input(INPUT_POST, "industry", FILTER_SANITIZE_NUMBER_INT);
	$strategy = htmlentities(strip_tags($_POST["strategy"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$people_culture = htmlentities(strip_tags($_POST["people_culture"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$sales_marketing = htmlentities(strip_tags($_POST["sales_marketing"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$operations = htmlentities(strip_tags($_POST["operations"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$technology = htmlentities(strip_tags($_POST["technology"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$environmental = htmlentities(strip_tags($_POST["environmental"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$financial = htmlentities(strip_tags($_POST["financial"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	if(!!$industry && !!$strategy && !!$people_culture && !!$sales_marketing && !!$operations && !!$technology && !!$environmental && !!$financial){
		require_once("../utility/config/database.php");
		$bestpracticedata = array($strategy, $people_culture, $sales_marketing, $operations, $technology, $environmental, $financial, $industry);
		$db = new dataHandler();
		$bestpracticeid = $db->updateIndustryBestPractice($bestpracticedata);
	}
	header("Location: bestpractices.php");