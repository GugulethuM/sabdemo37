<?php
    session_name("INDSightsAdmin");
    session_start();
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== TRUE){
        header("Location: login.php");
        $_SESSION["error"] = "<font color=red>You don't have privileges to see the admin page.</font>";
        exit();
    }
    require_once("../utility/config/database.php");
    $indb = new dataHandler();
    $barometer = $indb->getAdminBarometer($_GET["id"]);
    if(empty($barometer))
        header("Location: barometers.php");
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="robots" content="noindex, nofollow" />
    <title>Edit Industry Barometer | INDSights Admin</title>
    <base href="/admin/" />
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <style type="text/css">
		
        @font-face {
            font-family: 'Frank-Light';
            src: url("assets/fonts/frank/Frank-Light.eot");
            src: url("assets/fonts/frank/Frank-Light.eot#?iefix") format("embedded-opentype"), 
                url("assets/fonts/frank/Frank-Light.woff") format("woff"), 
                url("assets/fonts/frank/Frank-Light.ttf") format("truetype"), 
                url("assets/fonts/frank/Frank-Light.svg") format("svg");
            font-weight: normal;
        }
        
        @font-face {
            font-family: 'Frank-Regular';
            src: url("assets/fonts/frank/Frank-Regular.eot");
            src: url("assets/fonts/frank/Frank-Regular.eot#?iefix") format("embedded-opentype"), 
                url("assets/fonts/frank/Frank-Regular.woff") format("woff"), 
                url("assets/fonts/frank/Frank-Regular.ttf") format("truetype"), 
                url("assets/fonts/frank/Frank-Regular.svg") format("svg");
            font-weight: normal;
        }
        
        @font-face {
            font-family: 'Frank-Bold';
            src: url("assets/fonts/frank/FrankBol.eot");
            src: url("assets/fonts/frank/FrankBol.eot#?iefix") format("embedded-opentype"), 
                url("assets/fonts/frank/FrankBol.woff") format("woff"), 
                url("assets/fonts/frank/FrankBol.ttf") format("truetype"), 
                url("assets/fonts/frank/FrankBol.svg") format("svg");
            font-weight: normal;
        }

        html, body {height: 100%;}

        body {margin: 0; padding: 0; background-color: #ffffff; color: #000000; font-family: 'Frank-Regular', Arial, Helvetica, sans-serif;}
        :focus {outline: none;}

        .logo {margin: 0; box-sizing: border-box; border: 0 none; padding: 20px 15px; height: 120px;}
        .jpu__logo--graphic {margin: 0 10px 0 0; box-sizing: border-box; border: 0 none; padding: 0; position: relative; width: auto; height: 100%;}
        .jpu__logo--text {margin: 20px 0; box-sizing: border-box; border: 0 none; padding: 0; position: relative; width: auto; height: calc(100% - 40px);}

        h1, h2, h3, h4, h5, h6 {font-family: 'Frank-Bold', Arial, Helvetica, sans-serif;}
        .locationheader {/*margin-top: 20px; margin-bottom: 40px;*/ color: #bed731; /*text-decoration: underline;*/}

        /*#ff8000*/
        .nav-tabs {border-color: #bed731;}
        .nav-tabs .nav-link {font-family: 'Frank-Bold', Arial, Helvetica, sans-serif; color: #ffffff; border-radius: 0 !important;}
        .nav-tabs .nav-link:focus, .nav-tabs .nav-link:hover {border-color: #bed731; color: #bed731;}
        .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {border-color: #bed731 #bed731 #bed731; background-color: #bed731; color: #000000; font-weight: bold;}

        .tab-pane {padding-top: 50px; position: relative; display: block; width: 100%; height: calc(100vh - 162px);}

        .countrycontainer {margin: 0; box-sizing: border-box; padding: 0; position: absolute; top: 0; left: 0; display: flex; flex-direction: row; flex-wrap: nowrap; width: 100%; height: 100%;}
        
        /*.socialitem {margin: 0; position: relative; width: 20%; height: 50%;}*/
        .socialitem {flex-grow: 1; flex-basis: 20%;}

        .socialcontentholder {margin: 0; box-sizing: border-box; border: 0 none; padding: 0; position: relative; display: table; table-layout: fixed; width: 100%; height: 100%;}
        .socialcontent {margin: 0; box-sizing: border-box; border: 0 none; padding: 10px; position: relative; display: table-cell; width: 100%; height: 100%; text-align: left; vertical-align: middle;}

        .tweetcontainer {max-width: 100%;}
        /*.tweetcontainer iframe {height: auto !important;}*/

        a.contentlink {margin: 0; box-sizing: border-box; border: 0 none; padding: 0; position: relative; display: block; clear: both; color: #ffffff; line-height: 1.5em; text-decoration: none; transition: color 0.4s; overflow: hidden;}
        a.contentlink::after {content: ''; margin: 0; box-sizing: border-box; border: 0 none; padding: 0; position: absolute; bottom: 0; left: 0; display: block; clear: both; width: 0; height: 2px; background-color: #bed731; transition: width 0.4s;}
        a.contentlink:hover {color: #bed731; text-decoration: none;}
        a.contentlink:hover::after {width: 100%;}
        a.contentlink strong {font-size: 1.3rem;}
        a.contentlink img {margin: 0 10px 10px 0; display: block;}

        .contenttraffic {font-family: 'Frank-Light', Arial, Helvetica, sans-serif !important;}

        .videocontainer {margin: 0 auto 20px; box-sizing: border-box; padding: 0; position: relative; display: block; width: 100%; height: auto;}
        .videoholder {margin: 0; box-sizing: border-box; padding: 0 0 56.25%; position: relative; display: block; width: 100%; height: 0;}
        .videoholder iframe {margin: 0; box-sizing: border-box; border: 1px solid #ddd; padding: 0; position: absolute; top: 0; left: 0; display: block; width: 100%; height: 100% !important;}

        .jpu__logo--fill {fill: #ffffff;}
        
        .smci {padding-bottom: 25px;}
        .smci hr {background-color: #cccccc;}

        .modal-lg {width: 80vw; max-width: 80vw; height: 80vh; max-height: 80vh;}
        .modal-content {height: 80vh; max-height: 80vh;}
        .modal-body iframe {width: 100%; height: calc(80vh - 90px) !important;}
        
        @media screen and (max-width: 768px){
            .jpu__logo--text {margin: 30px 0; height: calc(100% - 60px);}
        }
    </style>
    <script src="assets/jquery/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/tinymce/tinymce.min.js"></script>
    <script>
        function isReady(form){
            if(form.id.value == "") {
                alert("Please enter an ID!");
                return false;
            }
        }

        function createslug(){
            var tgtslug = document.querySelector("#" + this.getAttribute("data-slugtgt"));
            if(tgtslug && !tgtslug.disabled){
                var slugvalue = this.value.toLowerCase()
                slugvalue = slugvalue.replace(/'/g, "");
                slugvalue = slugvalue.replace(/\W/g, "-");
                slugvalue = slugvalue.replace(/(\-{2,})|(\s+)/g, "-");
                slugvalue = slugvalue.substring(0, 80);
                slugvalue = slugvalue.replace(/^\-|\-$/g, "");
                tgtslug.value = slugvalue;
            }
        }
        
        function initpage(){
            var titlefield = document.querySelectorAll("input[data-slugtgt]");
            if(titlefield.length > 0){
                for(var tf = 0; tf < titlefield.length; ++tf){
                    titlefield[tf].addEventListener("keyup", createslug, false);
                    titlefield[tf].addEventListener("blur", createslug, false);
                }
            }
        }

        tinymce.init({
            // General options
            selector:                   "textarea.editmce",
            //images_upload_url:          "uploadImage.php",
            //images_upload_base_path:    "/some/basepath",
            images_upload_credentials:  false,
            height:                     225,
            plugins:                    ["fullscreen, code, link, lists"],
            resize:                     false,
            toolbar:                    "undo redo | bold italic | bullist numlist | link | code",
            relative_urls:              false,
            menubar:                    false
        });

        window.addEventListener("DOMContentLoaded", initpage, false);
	  </script>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col align-self-center">
            <h1>SAB INDSights Admin</h1>
        </div>
        <div class="col logo text-right">
            <a href="./" class="btn btn-dark">Home</a>
            <a href="logout.php" class="btn btn-danger">Logout</a>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col"><h2>Edit Barometer</h2></div>
        <div class="col text-right"><a href="barometers.php" class="btn btn-warning">Back</a></div>
    </div>
</div>

<div class="container mt-5">
    <div class="row">
        <div class="col">
            <form name="createBarometer" action="updateBarometer.php" method="post">
                <div class="form-group">
                    <input type="hidden" name="industry" value="<?php echo $barometer["industryid"]; ?>" />
                    <label for="headline">Industry</label>
                    <h3><?php echo $barometer["industry"]; ?></h3>
                </div>
                <div class="form-group">
                    <label for="substitution">Threat of Substitution</label>
                    <select name="substitution_level" style="float: right;" required>
                        <option value="">Select a level</option>
                        <option value="0"<?php if((int)$barometer["substitution_level"] === 0) echo " selected"; ?>>Low</option>
                        <option value="1"<?php if((int)$barometer["substitution_level"] === 1) echo " selected"; ?>>Medium</option>
                        <option value="2"<?php if((int)$barometer["substitution_level"] === 2) echo " selected"; ?>>High</option>
                    </select>
                    <textarea name="substitution" cols="70" rows="10" wrap="soft" id="substitution" class="form-control editmce"><?php echo html_entity_decode($barometer["substitution"]); ?></textarea>
                </div>
                <div class="form-group">
                    <label for="entry">Threat of New Entry</label>
                    <select name="entry_level" style="float: right;" required>
                        <option value="">Select a level</option>
                        <option value="0"<?php if((int)$barometer["entry_level"] === 0) echo " selected"; ?>>Low</option>
                        <option value="1"<?php if((int)$barometer["entry_level"] === 1) echo " selected"; ?>>Medium</option>
                        <option value="2"<?php if((int)$barometer["entry_level"] === 2) echo " selected"; ?>>High</option>
                    </select>
                    <textarea name="entry" cols="70" rows="10" wrap="soft" id="entry" class="form-control editmce"><?php echo html_entity_decode($barometer["entry"]); ?></textarea>
                </div>
                <div class="form-group">
                    <label for="rivalry">Competition Rivalry</label>
                    <select name="rivalry_level" style="float: right;" required>
                        <option value="">Select a level</option>
                        <option value="0"<?php if((int)$barometer["rivalry_level"] === 0) echo " selected"; ?>>Low</option>
                        <option value="1"<?php if((int)$barometer["rivalry_level"] === 1) echo " selected"; ?>>Medium</option>
                        <option value="2"<?php if((int)$barometer["rivalry_level"] === 2) echo " selected"; ?>>High</option>
                    </select>
                    <textarea name="rivalry" cols="70" rows="10" wrap="soft" id="rivalry" class="form-control editmce"><?php echo html_entity_decode($barometer["rivalry"]); ?></textarea>
                </div>
                <div class="form-group">
                    <label for="buyer">Buyer Power</label>
                    <select name="buyer_level" style="float: right;" required>
                        <option value="">Select a level</option>
                        <option value="0"<?php if((int)$barometer["buyer_level"] === 0) echo " selected"; ?>>Low</option>
                        <option value="1"<?php if((int)$barometer["buyer_level"] === 1) echo " selected"; ?>>Medium</option>
                        <option value="2"<?php if((int)$barometer["buyer_level"] === 2) echo " selected"; ?>>High</option>
                    </select>
                    <textarea name="buyer" cols="70" rows="10" wrap="soft" id="buyer" class="form-control editmce"><?php echo html_entity_decode($barometer["buyer"]); ?></textarea>
                </div>
                <div class="form-group">
                    <label for="supplier">Supplier Power</label>
                    <select name="supplier_level" style="float: right;" required>
                        <option value="">Select a level</option>
                        <option value="0"<?php if((int)$barometer["supplier_level"] === 0) echo " selected"; ?>>Low</option>
                        <option value="1"<?php if((int)$barometer["supplier_level"] === 1) echo " selected"; ?>>Medium</option>
                        <option value="2"<?php if((int)$barometer["supplier_level"] === 2) echo " selected"; ?>>High</option>
                    </select>
                    <textarea name="supplier" cols="70" rows="10" wrap="soft" id="supplier" class="form-control editmce"><?php echo html_entity_decode($barometer["supplier"]); ?></textarea>
                </div>
                <div class="form-group">
                    <div align="center"> 
                        <input type="submit" name="editbarometer" value="Edit Barometer" class="btn btn-primary" />
                        &nbsp; 
                        <input name="reset" type="reset" id="reset" value="Reset" class="btn btn-danger" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>