<?php
	//stripslashes
    //$industry = filter_input(INPUT_POST, "industry", FILTER_SANITIZE_NUMBER_INT);
    $title = htmlentities(strip_tags($_POST["title"]), ENT_HTML5, "UTF-8", FALSE);
	$industry_overview = htmlentities(strip_tags($_POST["industry_overview"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$product_service = htmlentities(strip_tags($_POST["product_service"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$current_situation = htmlentities(strip_tags($_POST["current_situation"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$complication = htmlentities(strip_tags($_POST["complication"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$project_objective = htmlentities(strip_tags($_POST["project_objective"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$approach = htmlentities(strip_tags($_POST["approach"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$outcomes = htmlentities(strip_tags($_POST["outcomes"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	if(!!$title && !!$industry_overview && !!$product_service && !!$current_situation && !!$complication && !!$project_objective && !!$approach && !!$outcomes){
		require_once("../utility/config/database.php");
		$businesscasedata = array($title, $industry_overview, $product_service, $current_situation, $complication, $project_objective, $approach, $outcomes);
		$db = new dataHandler();
		$businesscaseid = $db->addBusinessCase($businesscasedata);
	}
	header("Location: businesscases.php");