<?php
//use mysqli;

class dataHandler {
    private $db = NULL;
    private $dbhost = "localhost";
    private $dbuser = "sabdemo37";
    private $dbpass = "w$k!7uB8x=v!?dVb";
    private $dbtable = "sabdemo37db";

    public function __construct(){
        $this->db = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbtable);
        if($this->db->connect_errno){
            return FALSE;
        } else
            return TRUE;
    }

    public function loginUser($useremail = NULL, $userpass = NULL){
        $userdetail = $this->getUser($useremail);
        if(empty($userdetail) || !is_array($userdetail) || count($userdetail) < 1)
            return NULL;
        $hashedpw = hash("sha256", $userpass . $userdetail["createdate"]);
        if($hashedpw !== $userdetail["userpass"])
            return NULL;
        else{
            // Check if the IP address is blank - update, or does not match the existing recorded IP access point
            if(empty($userdetail["userip"])){
                $this->updateUserIP($userdetail["userid"], $_SERVER["REMOTE_ADDR"]);
            } else{
                if($userdetail["userip"] !== $_SERVER["REMOTE_ADDR"]){
                    return array("error"=>"Recorded login location does not match request location.");
                }
            }
            return $userdetail["username"];
        }
    }

    public function getArticles(){
        $articles = array();
        $articlesquery = $this->db->prepare("SELECT `id`, `headline` FROM `insights` WHERE `deleted`=0 ORDER BY `date_created` DESC");
        //$userquery->bind_param("d", 0);
        $articlesquery->execute();
        $articlesquery->bind_result($articleid, $headline);
        while($articlesquery->fetch()){
            array_push($articles, array("articleid"=>$articleid, "headline"=>$headline));
        }
        $articlesquery->close();
        return $articles;
    }

    public function getArticle($articleid = NULL){
        if(empty($articleid))
            return NULL;
        $articlequery = $this->db->prepare("SELECT `id`, `headline`, `description`, `body` FROM `insights` WHERE `id`=?");
        $articlequery->bind_param("d", $articleid);
        $articlequery->execute();
        $articlequery->bind_result($articleid, $headline, $description, $body);
        $articlequery->fetch();
        $article = array("articleid"=>$articleid, "headline"=>$headline, "description"=>$description, "body"=>$body);
        $articlequery->close();
        return $article;
    }

    public function getArticleData($articleid = NULL){
        if(empty($articleid))
            return NULL;
        $articlequery = $this->db->prepare("SELECT `id`, `headline`, `slug`, `keywords`, `description`, `body`, `active` FROM `insights` WHERE `id`=?");
        $articlequery->bind_param("d", $articleid);
        $articlequery->execute();
        $articlequery->bind_result($articleid, $headline, $slug, $keywords, $description, $body, $active);
        $articlequery->fetch();
        $article = array("articleid"=>$articleid, "headline"=>$headline, "slug"=>$slug, "keywords"=>$keywords, "description"=>$description, "body"=>$body, "active"=>$active);
        $articlequery->close();
        return $article;
    }

    public function getArticleList(){
        $articlesquery = $this->db->prepare("SELECT `id`, `headline`, `description`, `date_created` FROM `insights` WHERE `active`=1 AND `deleted`=0 ORDER BY `id` DESC");
        $articlesquery->execute();
        $articlesquery->bind_result($articleid, $headline, $description, $date);
        $articles = array();
        while($articlesquery->fetch()){
            array_push($articles, array("articleid"=>$articleid, "headline"=>$headline, "description"=>$description, "date"=>$date));
        }
        $articlesquery->close();
        return $articles;
    }

    public function addArticle($articledata = NULL){
        if(empty($articledata))
            return FALSE;
        $articlecreatequery = $this->db->prepare("INSERT INTO `insights` (`headline`, `slug`, `keywords`, `description`, `body`, `active`) VALUES (?, ?, ?, ?, ?, ?)");
        $articlecreatequery->bind_param("sssssd", $articledata[0], $articledata[1], $articledata[2], $articledata[3], $articledata[4], $articledata[5]);
        $articlecreatequery->execute();
        $articleid = $articlecreatequery->insert_id;
        $articlecreatequery->close();
        return $articleid;
    }

    public function updateArticle($articledata = NULL){
        if(empty($articledata))
            return FALSE;
        $articleupdatequery = $this->db->prepare("UPDATE `insights` SET `headline`=?, `keywords`=?, `description`=?, `body`=? WHERE `id`=?");
        $articleupdatequery->bind_param("ssssd", $articledata[1], $articledata[2], $articledata[3], $articledata[4], $articledata[0]);
        $articleupdatequery->execute();
        $articleupdatequery->close();
        return TRUE;
    }

    public function deleteArticle($articleeid = NULL){
        if(empty($articleeid))
            return FALSE;
        $deletedstatus = 1;
        $articledeletequery = $this->db->prepare("UPDATE `insights` SET `deleted`=?, `date_deleted`=NOW() WHERE `id`=?");
        $articledeletequery->bind_param("dd", $deletedstatus, $articleeid);
        $articledeletequery->execute();
        $articledeletequery->close();
        return TRUE;
    }

    private function updateUserIP($userid = NULL, $userip = NULL){
        $userquery = $this->db->prepare("UPDATE `users` SET `userip`=? WHERE `userid`=?");
        $userquery->bind_param("sd", $userip, $userid);
        $userquery->execute();
        $userquery->close();
        return;
    }

    public function getUsers(){
        $users = array();
        $usersquery = $this->db->prepare("SELECT `userid`, `username`, `useremail`, `lastlogin` FROM `users` ORDER BY `username` ASC");
        $usersquery->execute();
        $usersquery->bind_result($userid, $username, $useremail, $lastlogin);
        while($usersquery->fetch()){
            array_push($users, array("userid"=>$userid, "username"=>$username, "useremail"=>$useremail, "lastlogin"=>$lastlogin));
        }
        $usersquery->close();
        return $users;
    }

    public function addUser($username = NULL, $useremail = NULL, $usermessage = NULL, $usershowcase = NULL){
        if(empty($username) || empty($useremail))
            return FALSE;
        /*
         * Creating the password
         *      - the createddate is the salt, make the salt
         *      - hash the email address - substr a random 8 characters (this is the users password)
         *      - combine the pw and the salt for the hash
         */
        $createddate = date("Y-m-d H:i:s", time());
        $emailhash = hash("md5", $useremail);
        $pw = substr($emailhash, rand(0, (strlen($emailhash) - 8)), 8);
        $pwhash = hash("sha256", $pw . $createddate);
        $usercreatequery = $this->db->prepare("INSERT INTO `users` (`username`, `useremail`, `usermessage`, `usershowcase`, `userpass`, `createdate`) VALUES (?, ?, ?, ?, ?, ?)");
        $usercreatequery->bind_param("ssssss", $username, $useremail, $usermessage, $usershowcase, $pwhash, $createddate);
        $usercreatequery->execute();
        $usercreatequery->close();
        return $pw;
    }
}