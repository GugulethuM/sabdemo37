<?php
    //stripslashes
    $industry = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);
	if(!!$industry){
        require_once("../utility/config/database.php");
        $db = new dataHandler();
		$db->deleteIndustry($industry);
    }
	header("Location: industries.php");