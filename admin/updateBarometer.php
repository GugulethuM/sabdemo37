<?php
	//stripslashes
    $industry = filter_input(INPUT_POST, "industry", FILTER_SANITIZE_NUMBER_INT);
	$substitution = htmlentities(strip_tags($_POST["substitution"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$substitution_level = filter_input(INPUT_POST, "substitution_level", FILTER_SANITIZE_NUMBER_INT);
	$entry = htmlentities(strip_tags($_POST["entry"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$entry_level = filter_input(INPUT_POST, "entry_level", FILTER_SANITIZE_NUMBER_INT);
	$rivalry = htmlentities(strip_tags($_POST["rivalry"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$rivalry_level = filter_input(INPUT_POST, "rivalry_level", FILTER_SANITIZE_NUMBER_INT);
	$buyer = htmlentities(strip_tags($_POST["buyer"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$buyer_level = filter_input(INPUT_POST, "buyer_level", FILTER_SANITIZE_NUMBER_INT);
	$supplier = htmlentities(strip_tags($_POST["supplier"], "<p><b><strong><i><em><br><a><img><ul><ol><li><h1><h2><h3><h4><h5><h6>"), ENT_HTML5, "UTF-8", FALSE);
	$supplier_level = filter_input(INPUT_POST, "supplier_level", FILTER_SANITIZE_NUMBER_INT);
	if(!!$industry && !!$substitution && $substitution_level !== FALSE && !!$entry && $entry_level !== FALSE && !!$rivalry && $rivalry_level !== FALSE && !!$buyer && $buyer_level !== FALSE && !!$supplier && $supplier_level !== FALSE){
        require_once("../utility/config/database.php");
		$barometerdata = array($substitution, $substitution_level, $entry, $entry_level, $rivalry, $rivalry_level, $buyer, $buyer_level, $supplier, $supplier_level, $industry);
		$db = new dataHandler();
		$barometerid = $db->updateIndustryBarometer($barometerdata);
    }
	header("Location: barometers.php");