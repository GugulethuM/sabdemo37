<?php
    session_name("INDSights");
    session_start();

    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"])
        header("Location: indsight");
?><!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INDSights</title>
    <base href="/" />
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="js/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145584979-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    
    gtag('config', 'UA-145584979-1');
    </script>

</head>
<body>
    <navigation></navigation>
    <login ng-init="loginForm()"></login>
    <div class="container-fluid">
        
    </div>
    <div class="svg-login">
        <img src="images/svg/v2/triangle-w-lines2.svg" class="img">
    </div>

    <div class="modal fade" id="regmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4>Thank you. You have successfully registered.</h4>
                    <p>Look out for an email from us to verify your email address.</p>
                </div>
                <div class="modal-footer" style="border-top: 0 none !important;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="verifymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4>Thank you. You have been successfully verified.</h4>
                    <p>You can now log in.</p>
                </div>
                <div class="modal-footer" style="border-top: 0 none !important;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="verifymodalfalse" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4>Your verification has been unsuccessful.</h4>
                </div>
                <div class="modal-footer" style="border-top: 0 none !important;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/angular.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/gsap/TweenMax.js"></script>
    <script src="js/gsap/src/minified/plugins/CSSPlugin.min.js"></script>
    <script src="js/gsap/ScrollToPlugin.js"></script>
    <script src="js/main-ng.js"></script>
    <script src="js/ng-directives.js"></script>
    <script src="js/controllers/ContentController.js"></script>
</body>
</html>