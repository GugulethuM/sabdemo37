<?php
    session_name("INDSights");
    session_start();
    if(isset($_POST) && !empty($_POST)){
        $errorarray = array("errors"=>array());
        $valuearray = array();
        $created = date("Y-m-d H:i:s", time());

        $userid = filter_input(INPUT_POST, "userid", FILTER_SANITIZE_NUMBER_INT);
        if(!!$userid && empty($userid))
            array_push($errorarray["errors"], array("userid"=>"Malformed User ID."));
        else
            array_push($valuearray, $userid);

        // Validate/sanitize the field data
        $firstname = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_ENCODE_HIGH|FILTER_FLAG_ENCODE_AMP);
        if(!!$firstname && empty($firstname))
            array_push($errorarray["errors"], array("name"=>"Malformed Firstname."));
        else
            array_push($valuearray, $firstname);

        $company = filter_input(INPUT_POST, "company", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_ENCODE_HIGH|FILTER_FLAG_ENCODE_AMP);
        if(!!$company && empty($company))
            array_push($errorarray["errors"], array("surname"=>"Malformed Company name."));
        else
            array_push($valuearray, $company);

        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
        if(!!$email && empty($email))
            array_push($errorarray["errors"], array("email"=>"Malformed Email address."));
        else
            array_push($valuearray, $email);

        $phone = filter_input(INPUT_POST, "phone", FILTER_SANITIZE_NUMBER_INT);
        if(!!$phone && empty($phone))
            array_push($errorarray["errors"], array("phone"=>"Malformed Phone number."));
        else
            array_push($valuearray, $phone);

        $industry = filter_input(INPUT_POST, "industry", FILTER_SANITIZE_NUMBER_INT);
        if(!!$industry && empty($industry))
            array_push($errorarray["errors"], array("industry"=>"Malformed Industry."));
        else
            array_push($valuearray, $industry);

        /*$goods = filter_input(INPUT_POST, "goods", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_ENCODE_HIGH|FILTER_FLAG_ENCODE_AMP);
        if(!!$goods && empty($goods))
            array_push($errorarray["errors"], array("goods"=>"Malformed Goods."));
        else
            array_push($valuearray, $goods);*/
        $goods = "NULL";
        array_push($valuearray, $goods);

        if(empty($errorarray["errors"])){
            require_once("config/database.php");
            $updatedb = new dataHandler();
            $userid = $updatedb->updateUser($valuearray);
            if(is_numeric($userid)){
                $_SESSION["industry"] = $industry;
                $_SESSION["industryname"] = $updatedb->getIndustryName($industry);
				header("Content-type: application/json; charset=utf-8");
                http_response_code(200);
                exit(json_encode(array("success"=>$userid)));
            }
        } else{
			header("Content-type: application/json; charset=utf-8");
            http_response_code(500);
            exit(json_encode($errorarray));
        }
    } else{
        http_response_code(500);
        exit("No direct script access allowed");
    }