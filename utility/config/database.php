<?php
//use mysqli;

class dataHandler {
    private $db = NULL;
    private $dbhost = "localhost";
    private $dbuser = "sabdemo37";
    private $dbpass = 'w$k!7uB8x=v!?dVb';
    private $dbtable = "sabdemo37db";
    private $key = "HaP7fG3ZkVFTXJSqsQRp5BBk5NpAQEkxGzuusrAJMv3dvTN6CkXEgrhRXWNPhVZuhZWHvLtrZmMgSKK9vPdvK7ePwtKxDVLy6YMupJUsBR3pDUzfPgZJF4mSQfPxQgrf";

    public function __construct(){
        $this->db = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbtable);
        if($this->db->connect_errno){
            return FALSE;
        } else
            return TRUE;
    }

    public function loginAdmin($u = NULL, $p = NULL){
        if(empty($u) || empty($p))
            return FALSE;
        $adminuserdetail = $this->getAdminUserDetails($u);
        if(empty($adminuserdetail) || !is_array($adminuserdetail) || count($adminuserdetail) < 1)
            return FALSE;
        $hashedpw = hash("sha256", $p . $adminuserdetail["created"]);
        if($hashedpw !== $adminuserdetail["password"])
            return FALSE;
        else{
            if(isset($adminuserdetail['adminid']))
				$this->updateAdminUserLogin($adminuserdetail["adminid"]);
            return $adminuserdetail;
        }
    }

    private function getAdminUserDetails($u = NULL){
        if(empty($u))
            return NULL;
        $userquery = $this->db->prepare("SELECT `adminid`, `adminname`, `adminpass`, `createdate` FROM `indsights_admins` WHERE `adminhandle`=?");
        $userquery->bind_param("s", $u);
        $userquery->execute();
        $userquery->bind_result($id, $name, $password, $saltval);
        $userquery->fetch();
        $userdetails = array("id"=>$id, "name"=>$name, "password"=>$password, "created"=>$saltval);
        $userquery->close();
        return $userdetails;
    }

    public function registerUser($registrationdata = NULL){
        if(empty($registrationdata))
            return FALSE;

        if(!$this->checkExistingUser($registrationdata[2])){
            $registerquery = $this->db->prepare("INSERT INTO `indsights_users` (`name`, `company`, `email`, `phone`, `industry`, `goods`, `password`, `created`) VALUES (AES_ENCRYPT(?, '" . $this->key . "'), AES_ENCRYPT(?, '" . $this->key . "'), AES_ENCRYPT(?, '" . $this->key . "'), AES_ENCRYPT(?, '" . $this->key . "'), ?, ?, ?, ?)");
            $registerquery->bind_param("ssssdsss", $registrationdata[0], $registrationdata[1], $registrationdata[2], $registrationdata[3], $registrationdata[4], $registrationdata[5], $registrationdata[6], $registrationdata[7]);
            $registerquery->execute();
            $registerid = $registerquery->insert_id;
            $registerquery->close();

            $registerhash = md5($registerid);
            // Update the user
            $registerhashquery = $this->db->prepare("UPDATE `indsights_users` SET `vhash`=? WHERE `id`=?");
            $registerhashquery->bind_param("sd", $registerhash, $registerid);
            $registerhashquery->execute();
            $registerhashquery->close();
            
            return $registerid;
        } else
            return "An user with that email address has already been registered.";
    }

    private function checkExistingUser($useremail = NULL){
        $exists = FALSE;
        $userquery = $this->db->query("SELECT `indsights_users`.`id` FROM `indsights_users` WHERE `indsights_users`.`email`=AES_ENCRYPT('" . $useremail . "', '" . $this->key . "')");
        $usercount = $userquery->num_rows;
        $userquery->close();
        $exists = ($usercount > 0) ? TRUE : FALSE;
        return $exists;
    }

    public function verifyUser($hash = NULL){
        $vuserquery = $this->db->prepare("SELECT `id` FROM `indsights_users` WHERE `vhash`=? AND `verified`=0");
        $vuserquery->bind_param("s", $hash);
        $vuserquery->execute();
        $vuserquery->bind_result($id);
        $vuserquery->fetch();
        $vuserdetails = array("id"=>$id);
        $vuserquery->close();
        //$vuserid = $vuserdetails["id"];
        // Update the user
        $verifiedquery = $this->db->prepare("UPDATE `indsights_users` SET `verified`=1 WHERE `id`=?");
        $verifiedquery->bind_param("d", $vuserdetails["id"]);
        $verifiedquery->execute();
        $verifiedquery->close();
        return $vuserdetails["id"];
    }

    public function updateUser($updatedata = NULL){
        if(empty($updatedata))
            return FALSE;
        $updatequery = $this->db->prepare("UPDATE `indsights_users` SET `name`=AES_ENCRYPT(?, '" . $this->key . "'), `company`=AES_ENCRYPT(?, '" . $this->key . "'), `email`=AES_ENCRYPT(?, '" . $this->key . "'), `phone`=AES_ENCRYPT(?, '" . $this->key . "'), `industry`=?, `goods`=? WHERE `id`=?");
        $updatequery->bind_param("ssssisi", $updatedata[1], $updatedata[2], $updatedata[3], $updatedata[4], $updatedata[5], $updatedata[6], $updatedata[0]);
        $updatequery->execute();
        $updatequery->close();
        return $updatedata[0];
    }

    public function loginUser($logindata = NULL){
        $userdetail = $this->getUserDetails($logindata[0]);
        if(empty($userdetail) || !is_array($userdetail) || count($userdetail) < 1)
            return NULL;
        $hashedpw = hash("sha256", $logindata[1] . $userdetail["created"]);
        if($hashedpw !== $userdetail["password"])
            return NULL;
        else{
			if(isset($userdetail["id"]))
				$this->updateUserLogin($userdetail["id"]);
            return $userdetail;
        }
    }

    public function getAccountDetails($userid = NULL){
        if(empty($userid))
            return NULL;
        $userquery = $this->db->prepare("SELECT `id`, AES_DECRYPT(`name`, '" . $this->key . "'), AES_DECRYPT(`company`, '" . $this->key . "'), AES_DECRYPT(`email`, '" . $this->key . "'), AES_DECRYPT(`phone`, '" . $this->key . "'), `industry`, `goods` FROM `indsights_users` WHERE `id`=?");
        $userquery->bind_param("i", $userid);
        $userquery->execute();
        $userquery->bind_result($id, $name, $company, $email, $phone, $industry, $goods);
        $userquery->fetch();
        $userdetails = array("id"=>$id, "name"=>$name, "company"=>$company, "email"=>$email, "phone"=>$phone, "industry"=>$industry, "goods"=>$goods);
        $userquery->close();
        return $userdetails;
    }

    private function getUserDetails($useremail = NULL){
        if(empty($useremail))
            return NULL;
        if(!$userquery = $this->db->prepare("SELECT `indsights_users`.`id`, AES_DECRYPT(`indsights_users`.`name`, '" . $this->key . "'), AES_DECRYPT(`indsights_users`.`company`, '" . $this->key . "'), `indsights_users`.`industry`, `indsights_industries`.`industry` AS 'industryname', `indsights_users`.`password`, `indsights_users`.`created` FROM `indsights_users` LEFT JOIN `indsights_industries` ON `indsights_industries`.`id`=`indsights_users`.`industry` WHERE `indsights_users`.`email`=AES_ENCRYPT(?, '" . $this->key . "') AND `verified`=1 ORDER BY `indsights_users`.`id` ASC LIMIT 1"))
			echo "Error preparing statement: " . $this->db->error;
		if(!$userquery->bind_param('s', $useremail))
			echo "Error binding params: " . $userquery->error;
        if(!$userquery->execute())
			echo "Error binding params: " . $userquery->error;
        $userquery->bind_result($id, $name, $company, $industry, $industryname, $password, $saltval);
        $userquery->fetch();
        $userlogindetails = array("id"=>$id, "name"=>$name, "company"=>$company, "industry"=>$industry, "industryname"=>$industryname, "password"=>$password, "created"=>$saltval);
        $userquery->close();
        return $userlogindetails;
    }

    public function getIndustriesList(){
        $industries = array();
        //$industriesquery = $this->db->prepare("SELECT `id`, `industry` FROM `indsights_industries` ORDER BY `industry` ASC");
        $industriesquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry` FROM `indsights_industries` WHERE EXISTS(SELECT * FROM `indsights_industrysnapshot` WHERE `indsights_industrysnapshot`.`industry`=`indsights_industries`.`id`) AND EXISTS(SELECT * FROM `indsights_industrybarometer` WHERE `indsights_industrybarometer`.`industry`=`indsights_industries`.`id`) AND EXISTS(SELECT * FROM `indsights_industrybestpractices` WHERE `indsights_industrybestpractices`.`industry`=`indsights_industries`.`id`) ORDER BY `indsights_industries`.`industry` ASC");
        //$userquery->bind_param("d", 0);
        $industriesquery->execute();
        $industriesquery->bind_result($id, $industry);
        while($industriesquery->fetch()){
            array_push($industries, array("id"=>$id, "industry"=>$industry));
        }
        $industriesquery->close();
        return $industries;
    }

    public function getSnapshotIndustriesList(){
        $industries = array();
        $industriesquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry` FROM `indsights_industries` WHERE `indsights_industries`.`id` IN (SELECT `indsights_industrysnapshot`.`industry` FROM `indsights_industrysnapshot`) ORDER BY `industry` ASC");
        $industriesquery->execute();
        $industriesquery->bind_result($id, $industry);
        while($industriesquery->fetch()){
            array_push($industries, array("id"=>$id, "industry"=>$industry));
        }
        $industriesquery->close();
        return $industries;
    }

    public function getBarometerIndustriesList(){
        $industries = array();
        $industriesquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry` FROM `indsights_industries` WHERE `indsights_industries`.`id` IN (SELECT `indsights_industrybarometer`.`industry` FROM `indsights_industrybarometer`) ORDER BY `industry` ASC");
        $industriesquery->execute();
        $industriesquery->bind_result($id, $industry);
        while($industriesquery->fetch()){
            array_push($industries, array("id"=>$id, "industry"=>$industry));
        }
        $industriesquery->close();
        return $industries;
    }

    public function getBestPracticesIndustriesList(){
        $industries = array();
        $industriesquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry` FROM `indsights_industries` WHERE `indsights_industries`.`id` IN (SELECT `indsights_industrybestpractices`.`industry` FROM `indsights_industrybestpractices`) ORDER BY `industry` ASC");
        $industriesquery->execute();
        $industriesquery->bind_result($id, $industry);
        while($industriesquery->fetch()){
            array_push($industries, array("id"=>$id, "industry"=>$industry));
        }
        $industriesquery->close();
        return $industries;
    }

    public function getIndustryID($industryname = NULL){
        if(empty($industryname))
            return NULL;
        $industryquery = $this->db->prepare("SELECT `id` FROM `indsights_industries` WHERE `industry`=?");
        $industryquery->bind_param("s", $industryname);
        $industryquery->execute();
        $industryquery->bind_result($id);
        $industryquery->fetch();
        $industryid = $id;
        $industryquery->close();
        return $industryid;
    }

    public function getIndustryName($industryid = NULL){
        if(empty($industryid))
            return NULL;
        $industryquery = $this->db->prepare("SELECT `industry` FROM `indsights_industries` WHERE `id`=?");
        $industryquery->bind_param("i", $industryid);
        $industryquery->execute();
        $industryquery->bind_result($name);
        $industryquery->fetch();
        $industryname = $name;
        $industryquery->close();
        return $industryname;
    }

    /* INDUSTRIES */
    public function getAdminIndustriesList(){
        $industries = array();
        $industriesquery = $this->db->prepare("SELECT `id`, `industry` FROM `indsights_industries` ORDER BY `industry` ASC");
        //$userquery->bind_param("d", 0);
        $industriesquery->execute();
        $industriesquery->bind_result($id, $industry);
        while($industriesquery->fetch()){
            array_push($industries, array("id"=>$id, "industry"=>$industry));
        }
        $industriesquery->close();
        return $industries;
    }

    public function deleteIndustry($industryid = NULL){
        if(!empty($industryid)){
            $industryquery = $this->db->prepare("DELETE FROM `indsights_industries` WHERE `id`=" . $industryid);
            $industryquery->execute();
            $industryquery->close();
            return;
        }
    }

    public function addIndustry($industrydata = NULL){
        if(empty($industrydata))
            return FALSE;
        $industrycreatequery = $this->db->prepare("INSERT INTO `indsights_industries` (`industry`) VALUES (?)");
        $industrycreatequery->bind_param("s", $industrydata[0]);
        $industrycreatequery->execute();
        $industryid = $industrycreatequery->insert_id;
        $industrycreatequery->close();
        return $industryid;
    }
    /* END INDUSTRIES */

    /* INDSIGHTS */
    public function getAdminIndsightsIndustriesList(){
        $industries = array();
        $industriesquery = $this->db->prepare("SELECT `id`, `industry` FROM `indsights_industries` WHERE `id` NOT IN (SELECT DISTINCT(`indsights_industryindsight`.`industry`) FROM `indsights_industryindsight`) ORDER BY `industry` ASC");
        //$userquery->bind_param("d", 0);
        $industriesquery->execute();
        $industriesquery->bind_result($id, $industry);
        while($industriesquery->fetch()){
            array_push($industries, array("id"=>$id, "industry"=>$industry));
        }
        $industriesquery->close();
        return $industries;
    }

    public function getIndustryIndsight($industry = NULL){
        if(empty($industry))
            return NULL;
        $industryindsightquery = $this->db->prepare("SELECT `indsight` FROM `indsights_industryindsight` WHERE `industry`=$industry");
        //$userquery->bind_param("d", 0);
        $industryindsightquery->execute();
        $industryindsightquery->bind_result($indsight);
        $industryindsightquery->fetch();
        $industryindsight = $indsight;
        $industryindsightquery->close();
        return html_entity_decode($industryindsight, ENT_HTML5, "UTF-8");
    }

    public function getAdminIndsights(){
        //$indsightsquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`` FROM `indsights_industries` LEFT JOIN `` ON ``.``=``.`` ORDER BY `indsights_industries`.`` ASC");
        $indsightsquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry` FROM `indsights_industryindsight` LEFT JOIN `indsights_industries` ON `indsights_industryindsight`.`industry`=`indsights_industries`.`id` ORDER BY `indsights_industries`.`industry` ASC");
        $industries = array();
        $indsightsquery->execute();
        $indsightsquery->bind_result($industryid, $industry);
        while($indsightsquery->fetch()){
            array_push($industries, array("id"=>$industryid, "industry"=>$industry));
        }
        $indsightsquery->close();
        return $industries;
    }

    public function addIndustryIndsight($indsightdata = NULL){
        if(empty($indsightdata))
            return FALSE;
        $indsightcreatequery = $this->db->prepare("INSERT INTO `indsights_industryindsight` (`indsight`, `industry`) VALUES (?, ?)");
        $indsightcreatequery->bind_param("sd", $indsightdata[0], $indsightdata[1]);
        $indsightcreatequery->execute();
        $indsightid = $indsightcreatequery->insert_id;
        $indsightcreatequery->close();
        return $indsightid;
    }

    public function getAdminIndsight($indsightid = NULL){
        if(empty($indsightid))
            return NULL;
        $indsightquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry`, `indsights_industryindsight`.`indsight` FROM `indsights_industryindsight` LEFT JOIN `indsights_industries` ON `indsights_industryindsight`.`industry`=`indsights_industries`.`id` WHERE `indsights_industryindsight`.`industry`=$indsightid");
        $indsightquery->execute();
        $indsightquery->bind_result($industryid, $industry, $indsight);
        $indsightquery->fetch();
        $indsight = array("industryid"=>$industryid, "industry"=>$industry, "indsight"=>$indsight);
        $indsightquery->close();
        return $indsight;
    }

    public function updateIndustryIndsight($indsightdata = NULL){
        if(empty($indsightdata))
            return FALSE;
        $indsightupdatequery = $this->db->prepare("UPDATE `indsights_industryindsight` SET `indsight`=? WHERE `industry`=?");
        $indsightupdatequery->bind_param("sd", $indsightdata[0], $indsightdata[1]);
        $indsightupdatequery->execute();
        $indsightupdatequery->close();
        return TRUE;
    }
    /* END INDSIGHTS */

    /* SNAPSHOTS */
    public function getAdminSnapshotsIndustriesList(){
        $industries = array();
        $industriesquery = $this->db->prepare("SELECT `id`, `industry` FROM `indsights_industries` WHERE `id` NOT IN (SELECT DISTINCT(`indsights_industrysnapshot`.`industry`) FROM `indsights_industrysnapshot`) ORDER BY `industry` ASC");
        //$userquery->bind_param("d", 0);
        $industriesquery->execute();
        $industriesquery->bind_result($id, $industry);
        while($industriesquery->fetch()){
            array_push($industries, array("id"=>$id, "industry"=>$industry));
        }
        $industriesquery->close();
        return $industries;
    }
    
    public function getIndustrySnapshot($industry = NULL){
        if(empty($industry))
            return NULL;
        $industrysnapshotquery = $this->db->prepare("SELECT `political`, `economical`, `social`, `technological`, `legal`, `environmental` FROM `indsights_industrysnapshot` WHERE `industry`=$industry");
        //$userquery->bind_param("d", 0);
        $industrysnapshotquery->execute();
        $industrysnapshotquery->bind_result($political, $economical, $social, $technological, $legal, $environmental);
        $industrysnapshotquery->fetch();
        $industrysnapshot = array("political"=>html_entity_decode($political, ENT_HTML5, "UTF-8"), "economical"=>html_entity_decode($economical, ENT_HTML5, "UTF-8"), "social"=>html_entity_decode($social, ENT_HTML5, "UTF-8"), "technological"=>html_entity_decode($technological, ENT_HTML5, "UTF-8"), "legal"=>html_entity_decode($legal, ENT_HTML5, "UTF-8"), "environmental"=>html_entity_decode($environmental, ENT_HTML5, "UTF-8"));
        $industrysnapshotquery->close();
        return $industrysnapshot;
    }

    public function getAdminSnapshots(){
        $snapshotsquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry` FROM `indsights_industrysnapshot` LEFT JOIN `indsights_industries` ON `indsights_industrysnapshot`.`industry`=`indsights_industries`.`id` ORDER BY `indsights_industries`.`industry` ASC");
        $snapshots = array();
        $snapshotsquery->execute();
        $snapshotsquery->bind_result($industryid, $industry);
        while($snapshotsquery->fetch()){
            array_push($snapshots, array("id"=>$industryid, "industry"=>$industry));
        }
        $snapshotsquery->close();
        return $snapshots;
    }

    public function addIndustrySnapshot($snapshotdata = NULL){
        if(empty($snapshotdata))
            return FALSE;
        $snapshotcreatequery = $this->db->prepare("INSERT INTO `indsights_industrysnapshot` (`political`, `economical`, `social`, `technological`, `legal`, `environmental`, `industry`, `created`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        $snapshotcreatequery->bind_param("ssssssds", $snapshotdata[0], $snapshotdata[1], $snapshotdata[2], $snapshotdata[3], $snapshotdata[4], $snapshotdata[5], $snapshotdata[6], date("Y-m-d H:i:s", time()));
        $snapshotcreatequery->execute();
        $snapshotid = $snapshotcreatequery->insert_id;
        $snapshotcreatequery->close();
        return $snapshotid;
    }

    public function getAdminSnapshot($snapshotid = NULL){
        if(empty($snapshotid))
            return NULL;
        $snapshotquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry`, `indsights_industrysnapshot`.`political`, `indsights_industrysnapshot`.`economical`, `indsights_industrysnapshot`.`social`, `indsights_industrysnapshot`.`technological`, `indsights_industrysnapshot`.`legal`, `indsights_industrysnapshot`.`environmental` FROM `indsights_industrysnapshot` LEFT JOIN `indsights_industries` ON `indsights_industrysnapshot`.`industry`=`indsights_industries`.`id` WHERE `indsights_industrysnapshot`.`industry`=$snapshotid");
        $snapshotquery->execute();
        $snapshotquery->bind_result($industryid, $industry, $political, $economical, $social, $technological, $legal, $environmental);
        $snapshotquery->fetch();
        $snapshot = array("industryid"=>$industryid, "industry"=>$industry, "political"=>$political, "economical"=>$economical, "social"=>$social, "technological"=>$technological, "legal"=>$legal, "environmental"=>$environmental);
        $snapshotquery->close();
        return $snapshot;
    }

    public function updateIndustrySnapshot($snapshotdata = NULL){
        if(empty($snapshotdata))
            return FALSE;
        $snapshotupdatequery = $this->db->prepare("UPDATE `indsights_industrysnapshot` SET `political`=?, `economical`=?, `social`=?, `technological`=?, `legal`=?, `environmental`=? WHERE `industry`=?");
        $snapshotupdatequery->bind_param("ssssssd", $snapshotdata[0], $snapshotdata[1], $snapshotdata[2], $snapshotdata[3], $snapshotdata[4], $snapshotdata[5], $snapshotdata[6]);
        $snapshotupdatequery->execute();
        $snapshotupdatequery->close();
        return TRUE;
    }
    /* END SNAPSHOTS */

    /* BUSINESS CASES */
    public function searchBusinessCases($searchterm = NULL){
        if(empty($searchterm))
            return json_encode(array());
        $searchresults = array();
        //$searchquery = $this->db->prepare("SELECT `id`, `title` FROM `indsights_businesscases` WHERE REGEXP_LIKE(`title`, ?) OR REGEXP_LIKE(`product_service`, ?) OR REGEXP_LIKE(`current_situation`, ?) OR REGEXP_LIKE(`complication`, ?) OR REGEXP_LIKE(`project_objective`, ?) OR REGEXP_LIKE(`approach`, ?) OR REGEXP_LIKE(`outcomes`, ?)");
        $searchquery = $this->db->prepare("SELECT `id`, `title` FROM `indsights_businesscases` WHERE `title` REGEXP ? OR `industry_overview` REGEXP ? OR `product_service` REGEXP ? OR `current_situation` REGEXP ? OR `complication` REGEXP ? OR `project_objective` REGEXP ? OR `approach` REGEXP ? OR `outcomes` REGEXP ?");
        $searchquery->bind_param("ssssssss", $searchterm, $searchterm, $searchterm, $searchterm , $searchterm , $searchterm, $searchterm, $searchterm);
        $searchquery->execute();
        $searchquery->bind_result($id, $title);
        while($searchquery->fetch()){
            array_push($searchresults, array("id"=>$id, "label"=>$title, "value"=>$title));
        }
        $searchquery->close();
        return $searchresults;
    }
    
    public function getBusinessCase($businesscase = NULL){
        if(empty($businesscase))
            return NULL;
        $businesscasequery = $this->db->prepare("SELECT `indsights_businesscases`.`title`, `indsights_businesscases`.`industry_overview`, `indsights_businesscases`.`product_service`, `indsights_businesscases`.`current_situation`, `indsights_businesscases`.`complication`, `indsights_businesscases`.`project_objective`, `indsights_businesscases`.`approach`, `indsights_businesscases`.`outcomes` FROM `indsights_businesscases` WHERE `indsights_businesscases`.`id`=?");
        $businesscasequery->bind_param("i", $businesscase);
        $businesscasequery->execute();
        $businesscasequery->bind_result($title, $industry_overview, $product_service, $current_situation, $complication, $project_objective, $approach, $outcomes);
        $businesscasequery->fetch();
        $businesscasedata = array("title"=>$title, "industry_overview"=>html_entity_decode($industry_overview, ENT_HTML5, "UTF-8"), "product_service"=>html_entity_decode($product_service, ENT_HTML5, "UTF-8"), "current_situation"=>html_entity_decode($current_situation, ENT_HTML5, "UTF-8"), "complication"=>html_entity_decode($complication, ENT_HTML5, "UTF-8"), "project_objective"=>html_entity_decode($project_objective, ENT_HTML5, "UTF-8"), "approach"=>html_entity_decode($approach, ENT_HTML5, "UTF-8"), "outcomes"=>html_entity_decode($outcomes, ENT_HTML5, "UTF-8"));
        $businesscasequery->close();
        return $businesscasedata;
    }

    public function getAdminBusinessCases(){
        $businesscasesquery = $this->db->prepare("SELECT `indsights_businesscases`.`id`, `indsights_businesscases`.`title` FROM `indsights_businesscases` ORDER BY `indsights_businesscases`.`title` ASC");
        $businesscases = array();
        $businesscasesquery->execute();
        $businesscasesquery->bind_result($id, $title);
        while($businesscasesquery->fetch()){
            array_push($businesscases, array("id"=>$id, "title"=>$title));
        }
        $businesscasesquery->close();
        return $businesscases;
    }

    public function addBusinessCase($businesscasedata = NULL){
        if(empty($businesscasedata))
            return FALSE;
        $businesscasecreatequery = $this->db->prepare("INSERT INTO `indsights_businesscases` (`title`, `industry_overview`, `product_service`, `current_situation`, `complication`, `project_objective`, `approach`, `outcomes`, `created`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $businesscasecreatequery->bind_param("sssssssss", $businesscasedata[0], $businesscasedata[1], $businesscasedata[2], $businesscasedata[3], $businesscasedata[4], $businesscasedata[5], $businesscasedata[6], $businesscasedata[7], date("Y-m-d H:i:s", time()));
        $businesscasecreatequery->execute();
        $businesscaseid = $businesscasecreatequery->insert_id;
        $businesscasecreatequery->close();
        return $businesscaseid;
    }

    public function getAdminBusinessCase($businesscaseid = NULL){
        if(empty($businesscaseid))
            return NULL;
        $businesscasequery = $this->db->prepare("SELECT `indsights_businesscases`.`id`, `indsights_businesscases`.`title`, `indsights_businesscases`.`industry_overview`, `indsights_businesscases`.`product_service`, `indsights_businesscases`.`current_situation`, `indsights_businesscases`.`complication`, `indsights_businesscases`.`project_objective`, `indsights_businesscases`.`approach`, `indsights_businesscases`.`outcomes` FROM `indsights_businesscases` WHERE `indsights_businesscases`.`id`=$businesscaseid");
        $businesscasequery->execute();
        $businesscasequery->bind_result($id, $title, $industry_overview, $product_service, $current_situation, $complication, $project_objective, $approach, $outcomes);
        $businesscasequery->fetch();
        $businesscase = array("id"=>$id, "title"=>$title, "industry_overview"=>$industry_overview, "product_service"=>$product_service, "current_situation"=>$current_situation, "complication"=>$complication, "project_objective"=>$project_objective, "approach"=>$approach, "outcomes"=>$outcomes);
        $businesscasequery->close();
        return $businesscase;
    }

    public function updateBusinessCase($businesscasedata = NULL){
        if(empty($businesscasedata))
            return FALSE;
        $businesscaseupdatequery = $this->db->prepare("UPDATE `indsights_businesscases` SET `title`=?, `industry_overview`=?, `product_service`=?, `current_situation`=?, `complication`=?, `project_objective`=?, `approach`=?, `outcomes`=? WHERE `id`=?");
        $businesscaseupdatequery->bind_param("ssssssssi", $businesscasedata[0], $businesscasedata[1], $businesscasedata[2], $businesscasedata[3], $businesscasedata[4], $businesscasedata[5], $businesscasedata[6], $businesscasedata[7], $businesscasedata[8]);
        $businesscaseupdatequery->execute();
        $businesscaseupdatequery->close();
        return TRUE;
    }
    /* END BUSINESS CASES */

    /* BAROMETERS */
    public function getAdminBarometersIndustriesList(){
        $industries = array();
        $industriesquery = $this->db->prepare("SELECT `id`, `industry` FROM `indsights_industries` WHERE `id` NOT IN (SELECT DISTINCT(`indsights_industrybarometer`.`industry`) FROM `indsights_industrybarometer`) ORDER BY `industry` ASC");
        //$userquery->bind_param("d", 0);
        $industriesquery->execute();
        $industriesquery->bind_result($id, $industry);
        while($industriesquery->fetch()){
            array_push($industries, array("id"=>$id, "industry"=>$industry));
        }
        $industriesquery->close();
        return $industries;
    }
    
    public function getIndustryBarometer($industry = NULL){
        if(empty($industry))
            return NULL;
        $industrybarometerquery = $this->db->prepare("SELECT `substitution`, `substitution_level`, `entry`, `entry_level`, `rivalry`, `rivalry_level`, `buyer`, `buyer_level`, `supplier`, `supplier_level` FROM `indsights_industrybarometer` WHERE `industry`=$industry");
        //$userquery->bind_param("d", 0);
        $industrybarometerquery->execute();
        $industrybarometerquery->bind_result($substitution, $substitution_level, $entry, $entry_level, $rivalry, $rivalry_level, $buyer, $buyer_level, $supplier, $supplier_level);
        $industrybarometerquery->fetch();
        $industrybarometer = array("substitution"=>html_entity_decode($substitution, ENT_HTML5, "UTF-8"), "substitution_level"=>$substitution_level, "entry"=>html_entity_decode($entry, ENT_HTML5, "UTF-8"), "entry_level"=>$entry_level, "rivalry"=>html_entity_decode($rivalry, ENT_HTML5, "UTF-8"), "rivalry_level"=>$rivalry_level, "buyer"=>html_entity_decode($buyer, ENT_HTML5, "UTF-8"), "buyer_level"=>$buyer_level, "supplier"=>html_entity_decode($supplier, ENT_HTML5, "UTF-8"), "supplier_level"=>$supplier_level);
        $industrybarometerquery->close();
        return $industrybarometer;
    }

    public function getAdminBarometers(){
        $barometersquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry` FROM `indsights_industrybarometer` LEFT JOIN `indsights_industries` ON `indsights_industrybarometer`.`industry`=`indsights_industries`.`id` ORDER BY `indsights_industries`.`industry` ASC");
        $barometers = array();
        $barometersquery->execute();
        $barometersquery->bind_result($industryid, $industry);
        while($barometersquery->fetch()){
            array_push($barometers, array("id"=>$industryid, "industry"=>$industry));
        }
        $barometersquery->close();
        return $barometers;
    }

    public function addIndustryBarometer($barometerdata = NULL){
        if(empty($barometerdata))
            return FALSE;
        $barometercreatequery = $this->db->prepare("INSERT INTO `indsights_industrybarometer` (`substitution`, `substitution_level`, `entry`, `entry_level`, `rivalry`, `rivalry_level`, `buyer`, `buyer_level`, `supplier`, `supplier_level`, `industry`, `created`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $barometercreatequery->bind_param("sisisisisiis", $barometerdata[0], $barometerdata[1], $barometerdata[2], $barometerdata[3], $barometerdata[4], $barometerdata[5], $barometerdata[6], $barometerdata[7], $barometerdata[8], $barometerdata[9], $barometerdata[10], date("Y-m-d H:i:s", time()));
        $barometercreatequery->execute();
        $barometerid = $barometercreatequery->insert_id;
        $barometercreatequery->close();
        return $barometerid;
    }

    public function getAdminBarometer($barometerid = NULL){
        if(empty($barometerid))
            return NULL;
        $barometerquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry`, `indsights_industrybarometer`.`substitution`, `indsights_industrybarometer`.`substitution_level`, `indsights_industrybarometer`.`entry`, `indsights_industrybarometer`.`entry_level`, `indsights_industrybarometer`.`rivalry`, `indsights_industrybarometer`.`rivalry_level`, `indsights_industrybarometer`.`buyer`, `indsights_industrybarometer`.`buyer_level`, `indsights_industrybarometer`.`supplier`, `indsights_industrybarometer`.`supplier_level` FROM `indsights_industrybarometer` LEFT JOIN `indsights_industries` ON `indsights_industrybarometer`.`industry`=`indsights_industries`.`id` WHERE `indsights_industrybarometer`.`industry`=$barometerid");
        $barometerquery->execute();
        $barometerquery->bind_result($industryid, $industry, $substitution, $substitution_level, $entry, $entry_level, $rivalry, $rivalry_level, $buyer, $buyer_level, $supplier, $supplier_level);
        $barometerquery->fetch();
        $barometer = array("industryid"=>$industryid, "industry"=>$industry, "substitution"=>$substitution, "substitution_level"=>$substitution_level, "entry"=>$entry, "entry_level"=>$entry_level, "rivalry"=>$rivalry, "rivalry_level"=>$rivalry_level, "buyer"=>$buyer, "buyer_level"=>$buyer_level, "supplier"=>$supplier, "supplier_level"=>$supplier_level);
        $barometerquery->close();
        return $barometer;
    }

    public function updateIndustryBarometer($barometerdata = NULL){
        if(empty($barometerdata))
            return FALSE;
        $barometerupdatequery = $this->db->prepare("UPDATE `indsights_industrybarometer` SET `substitution`=?, `substitution_level`=?, `entry`=?, `entry_level`=?, `rivalry`=?, `rivalry_level`=?, `buyer`=?, `buyer_level`=?, `supplier`=?, `supplier_level`=? WHERE `industry`=?");
        $barometerupdatequery->bind_param("sisisisisii", $barometerdata[0], $barometerdata[1], $barometerdata[2], $barometerdata[3], $barometerdata[4], $barometerdata[5], $barometerdata[6], $barometerdata[7], $barometerdata[8], $barometerdata[9], $barometerdata[10]);
        $barometerupdatequery->execute();
        $barometerupdatequery->close();
        return TRUE;
    }
    /* END BAROMETERS */

    /* BEST PRACTICES */
    public function getAdminBestPracticesIndustriesList(){
        $industries = array();
        $industriesquery = $this->db->prepare("SELECT `id`, `industry` FROM `indsights_industries` WHERE `id` NOT IN (SELECT DISTINCT(`indsights_industrybestpractices`.`industry`) FROM `indsights_industrybestpractices`) ORDER BY `industry` ASC");
        //$userquery->bind_param("d", 0);
        $industriesquery->execute();
        $industriesquery->bind_result($id, $industry);
        while($industriesquery->fetch()){
            array_push($industries, array("id"=>$id, "industry"=>$industry));
        }
        $industriesquery->close();
        return $industries;
    }
    
    public function getIndustryBestPractice($industry = NULL){
        if(empty($industry))
            return NULL;
        $industrybestpracticequery = $this->db->prepare("SELECT `strategy`, `people_culture`, `sales_marketing`, `operations`, `technology`, `environmental`, `financial` FROM `indsights_industrybestpractices` WHERE `industry`=$industry");
        //$userquery->bind_param("d", 0);
        $industrybestpracticequery->execute();
        $industrybestpracticequery->bind_result($strategy, $people_culture, $sales_marketing, $operations, $technology, $environmental, $financial);
        $industrybestpracticequery->fetch();
        $industrybestpractice = array("strategy"=>html_entity_decode($strategy, ENT_HTML5, "UTF-8"), "people_culture"=>html_entity_decode($people_culture, ENT_HTML5, "UTF-8"), "sales_marketing"=>html_entity_decode($sales_marketing, ENT_HTML5, "UTF-8"), "operations"=>html_entity_decode($operations, ENT_HTML5, "UTF-8"), "technology"=>html_entity_decode($technology, ENT_HTML5, "UTF-8"), "environmental"=>html_entity_decode($environmental, ENT_HTML5, "UTF-8"), "financial"=>html_entity_decode($financial, ENT_HTML5, "UTF-8"));
        $industrybestpracticequery->close();
        return $industrybestpractice;
    }

    public function getAdminBestPractices(){
        $bestpracticesquery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry` FROM `indsights_industrybestpractices` LEFT JOIN `indsights_industries` ON `indsights_industrybestpractices`.`industry`=`indsights_industries`.`id` ORDER BY `indsights_industries`.`industry` ASC");
        $bestpractices = array();
        $bestpracticesquery->execute();
        $bestpracticesquery->bind_result($industryid, $industry);
        while($bestpracticesquery->fetch()){
            array_push($bestpractices, array("id"=>$industryid, "industry"=>$industry));
        }
        $bestpracticesquery->close();
        return $bestpractices;
    }

    public function addIndustryBestPractice($bestpracticedata = NULL){
        if(empty($bestpracticedata))
            return FALSE;
        $bestpracticecreatequery = $this->db->prepare("INSERT INTO `indsights_industrybestpractices` (`strategy`, `people_culture`, `sales_marketing`, `operations`, `technology`, `environmental`, `financial`, `industry`, `created`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $bestpracticecreatequery->bind_param("sssssssis", $bestpracticedata[0], $bestpracticedata[1], $bestpracticedata[2], $bestpracticedata[3], $bestpracticedata[4], $bestpracticedata[5], $bestpracticedata[6], $bestpracticedata[7], date("Y-m-d H:i:s", time()));
        $bestpracticecreatequery->execute();
        $bestpracticeid = $bestpracticecreatequery->insert_id;
        $bestpracticecreatequery->close();
        return $bestpracticeid;
    }

    public function getAdminBestPractice($bestpracticeid = NULL){
        if(empty($bestpracticeid))
            return NULL;
        $bestpracticequery = $this->db->prepare("SELECT `indsights_industries`.`id`, `indsights_industries`.`industry`, `indsights_industrybestpractices`.`strategy`, `indsights_industrybestpractices`.`people_culture`, `indsights_industrybestpractices`.`sales_marketing`, `indsights_industrybestpractices`.`operations`, `indsights_industrybestpractices`.`technology`, `indsights_industrybestpractices`.`environmental`, `indsights_industrybestpractices`.`financial` FROM `indsights_industrybestpractices` LEFT JOIN `indsights_industries` ON `indsights_industrybestpractices`.`industry`=`indsights_industries`.`id` WHERE `indsights_industrybestpractices`.`industry`=$bestpracticeid");
        $bestpracticequery->execute();
        $bestpracticequery->bind_result($industryid, $industry, $strategy, $people_culture, $sales_marketing, $operations, $technology, $environmental, $financial);
        $bestpracticequery->fetch();
        $bestpractice = array("industryid"=>$industryid, "industry"=>$industry, "strategy"=>$strategy, "people_culture"=>$people_culture, "sales_marketing"=>$sales_marketing, "operations"=>$operations, "technology"=>$technology, "environmental"=>$environmental, "financial"=>$financial);
        $bestpracticequery->close();
        return $bestpractice;
    }

    public function updateIndustryBestPractice($bestpracticedata = NULL){
        if(empty($bestpracticedata))
            return FALSE;
        $bestpracticeupdatequery = $this->db->prepare("UPDATE `indsights_industrybestpractices` SET `strategy`=?, `people_culture`=?, `sales_marketing`=?, `operations`=?, `technology`=?, `environmental`=?, `financial`=? WHERE `industry`=?");
        $bestpracticeupdatequery->bind_param("sssssssi", $bestpracticedata[0], $bestpracticedata[1], $bestpracticedata[2], $bestpracticedata[3], $bestpracticedata[4], $bestpracticedata[5], $bestpracticedata[6], $bestpracticedata[7]);
        $bestpracticeupdatequery->execute();
        $bestpracticeupdatequery->close();
        return TRUE;
    }
    /* END BEST PRACTICES */

    private function updateAdminUserLogin($userid = NULL){
        $userquery = $this->db->prepare("UPDATE `indsights_admins` SET `lastlogin`=NOW() WHERE `adminid`=?");
        $userquery->bind_param("i", $userid);
        $userquery->execute();
        $userquery->close();
        return;
    }

    private function updateUserLogin($userid = NULL){
        $userquery = $this->db->prepare("UPDATE `indsights_users` SET `lastlogin`=NOW() WHERE `id`=?");
        $userquery->bind_param("d", $userid);
        $userquery->execute();
        $userquery->close();
        return;
    }

    public function getUsers(){
        $users = array();
        $usersquery = $this->db->prepare("SELECT `userid`, `username`, `useremail`, `lastlogin` FROM `users` ORDER BY `username` ASC");
        $usersquery->execute();
        $usersquery->bind_result($userid, $username, $useremail, $lastlogin);
        while($usersquery->fetch()){
            array_push($users, array("userid"=>$userid, "username"=>$username, "useremail"=>$useremail, "lastlogin"=>$lastlogin));
        }
        $usersquery->close();
        return $users;
    }

    public function addUser($username = NULL, $useremail = NULL, $usermessage = NULL, $usershowcase = NULL){
        if(empty($username) || empty($useremail))
            return FALSE;
        /*
         * Creating the password
         *      - the createddate is the salt, make the salt
         *      - hash the email address - substr a random 8 characters (this is the users password)
         *      - combine the pw and the salt for the hash
         */
        $createddate = date("Y-m-d H:i:s", time());
        $emailhash = hash("md5", $useremail);
        $pw = substr($emailhash, rand(0, (strlen($emailhash) - 8)), 8);
        $pwhash = hash("sha256", $pw . $createddate);
        $usercreatequery = $this->db->prepare("INSERT INTO `users` (`username`, `useremail`, `usermessage`, `usershowcase`, `userpass`, `createdate`) VALUES (?, ?, ?, ?, ?, ?)");
        $usercreatequery->bind_param("ssssss", $username, $useremail, $usermessage, $usershowcase, $pwhash, $createddate);
        $usercreatequery->execute();
        $usercreatequery->close();
        return $pw;
    }

    public function getAdminUsers($search = NULL){
        $fsearch = "'%" . $search . "%'";
        $users = array();
        $uq = sprintf("SELECT `id`, CAST(AES_DECRYPT(`name`, '" . $this->key . "') AS CHAR(255)) decryptname, CAST(AES_DECRYPT(`email`, '" . $this->key . "') AS CHAR(255)) decryptemail, `vhash`, `verified` FROM `indsights_users` HAVING decryptname LIKE %s OR decryptemail LIKE %s", $fsearch, $fsearch);
        //$usersquery = $this->db->prepare("SELECT `id`, CAST(AES_DECRYPT(`name`, '" . $this->key . "') AS CHAR(255)) decryptname, CAST(AES_DECRYPT(`email`, '" . $this->key . "') AS CHAR(255)) decryptemail, `vhash`, `verified` FROM `indsights_users` HAVING decryptname LIKE ? OR decryptemail LIKE ?");
        $usersquery = $this->db->prepare($uq);
        //$usersquery->bind_param("ss", $fsearch, $fsearch);
        $usersquery->execute();
        $usersquery->bind_result($userid, $username, $useremail, $hash, $verified);
        while($usersquery->fetch()){
            array_push($users, array("id"=>$userid, "name"=>$username, "email"=>$useremail, "hash"=>$hash, "verified"=>$verified));
        }
        $usersquery->close();
        return $users;
    }

    public function deleteAdminUsers($uid = NULL){
        $usersquery = $this->db->prepare("DELETE FROM `indsights_users` WHERE `id`=?");
        $usersquery->bind_param("i", $uid);
        $usersquery->execute();
        return;
    }
}
