<?php
    if(!isset($_GET["term"]) || empty($_GET["term"]))
        exit(json_encode(array()));
    else{
        require_once("config/database.php");
        $search = new dataHandler();
        $searchterm = htmlentities(strip_tags($_GET["term"]), ENT_HTML5, "UTF-8", FALSE);
        $searchresults = $search->searchBusinessCases($searchterm);
        exit(json_encode($searchresults));
    }