<?php
    if(isset($_POST) && !empty($_POST)){
        $errorarray = array("errors"=>array());
        $valuearray = array();

        date_default_timezone_set("Africa/Johannesburg");
        $created = date("Y-m-d H:i:s", time());

        // Validate/sanitize the field data
        $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_ENCODE_HIGH|FILTER_FLAG_ENCODE_AMP);
        if(!!$name && empty($name))
            array_push($errorarray["errors"], array("name"=>"Malformed Name."));
        else
            array_push($valuearray, $name);

        $company = filter_input(INPUT_POST, "company", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_ENCODE_HIGH|FILTER_FLAG_ENCODE_AMP);
        if(!!$company && empty($company))
            array_push($errorarray["errors"], array("company"=>"Malformed Company name."));
        else
            array_push($valuearray, $company);

        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
        if(!!$email && empty($email))
            array_push($errorarray["errors"], array("email"=>"Malformed Email address."));
        else
            array_push($valuearray, $email);

        $phone = filter_input(INPUT_POST, "phone", FILTER_SANITIZE_NUMBER_INT);
        if(!!$phone && empty($phone))
            array_push($errorarray["errors"], array("phone"=>"Malformed Phone number."));
        else
            array_push($valuearray, $phone);

        $industry = filter_input(INPUT_POST, "industry", FILTER_SANITIZE_NUMBER_INT);
        if(!!$industry && empty($industry))
            array_push($errorarray["errors"], array("industry"=>"Malformed Industry."));
        else
            array_push($valuearray, $industry);

        /*$goods = filter_input(INPUT_POST, "goods", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_BACKTICK|FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_ENCODE_HIGH|FILTER_FLAG_ENCODE_AMP);
        if(!!$goods && empty($goods))
            array_push($errorarray["errors"], array("goods"=>"Malformed Goods."));
        else
            array_push($valuearray, $goods);*/
        $goods = "NULL";
        array_push($valuearray, $goods);

        $password = filter_input(INPUT_POST, "password", FILTER_UNSAFE_RAW);
        if(!!$password && empty($password))
            array_push($errorarray["errors"], array("password"=>"Malformed Password."));
        else{
            $hashedpw = hash("sha256", $password . $created);
            array_push($valuearray, $hashedpw);
        }

        if(empty($errorarray["errors"])){
            array_push($valuearray, $created);
            require_once("config/database.php");
            $regdb = new dataHandler();
            $userid = $regdb->registerUser($valuearray);
            if(is_numeric($userid)){

                // Send the verification email here
                $siteurl = ((isset($_SERVER["REQUEST_SCHEME"])) ? $_SERVER["REQUEST_SCHEME"] : "http") . "://" . $_SERVER['HTTP_HOST'];
                $verifykey = md5($userid);
                $messagebody = <<<REGEMAIL
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
        <o:AllowPNG />
        <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <!--[if !mso]><!-->
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <!--<![endif]-->
    <title>Expedia :: Your Perfect Blend of Macao</title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
    <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->
    <!-- CSS Reset -->
    <!-- Progressive Enhancements -->
    <style type="text/css">
        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,body {
            margin:0 !important;
            padding:0 !important;
            width:100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust:100%;
            -webkit-text-size-adjust:100%;
        }
        
        /* What it does: Forces Outlook.com to display emails full width. */
        .ExternalClass {
            width:100%;
        }
        
        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,td {
            mso-table-lspace:0 !important;
            mso-table-rspace:0 !important;
        }
        
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing:0 !important;
            border-collapse:collapse !important;
            table-layout:fixed !important;
            margin:0 auto !important;
        }
        
        table table table {
            table-layout:auto;
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: Overrides styles added when Yahoo's auto-senses a link. */
        .yshortcuts a {
            border-bottom:none !important;
        }
        
        /* What it does: Another work-around for iOS meddling in triggered links. */
        a[x-apple-data-detectors] {
            color:inherit !important;
        }
        
        /* What it does: Hover styles for buttons */
        .button-td,.button-a {
            transition:all 100ms ease-in;
        }
        
        .button-td:hover,.button-a:hover {
            background:#555555 !important;
            border-color:#555555 !important;
        }
        
        .show-mobi {
            display:none;
        }
        
        .table-2column {
            width:49% !important;
        }
        
        .table-2column.fl {
            float:right !important;
        }

        .table-3column {
            display: block !important;
            width:225px !important;
        }

        .table-3column.fl {
            float:left !important;
        }
        
        .footer-social {
            float:right;
        }
            
        /* Media Queries */
        @media screen and (max-width: 600px) {
            .email-container,.tbl-footer-tr,.table-2column,.table-3column,.left-text-container,.right-button-container {
                width:100% !important;
            }
            
            .table-2column.fl {
                float:left !important;
                height:auto !important;
            }
            
            .tbl-footer-tr {
                padding:30px 20px !important;
            }
            
            .fullwidthbutton {
                margin:0 auto 0 0 !important;
                float:left !important;
            }
            
            .footer-social {
                float:left;
            }
            
            .tdfixedheight {
                height:auto !important;
            }
            
            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,.fluid-centered {
                max-width:100% !important;
                height:auto !important;
                margin-left:auto !important;
                margin-right:auto !important;
            }
            
            /* And center justify these ones. */
            .fluid-centered {
                margin-left:auto !important;
                margin-right:auto !important;
            }
            
            .headers {
                margin-bottom:5px !important;
                width:auto !important;
                height:20px !important;
                max-width:100%;
                max-height:20px !important;
            }
            
            .textheader {
                font-size:16px !important;
            }
            
            /* What it does: Forces table cells into full-width rows. */
            .stack-column,.stack-column-center {
                display:block !important;
                width:100% !important;
                max-width:100% !important;
                direction:ltr !important;
            }
            
            /* And center justify these ones. */
            .stack-column-center {
                text-align:center !important;
            }
            
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align:center !important;
                display:block !important;
                margin-left:auto !important;
                margin-right:auto !important;
                float:none !important;
            }
            
            table.center-on-narrow {
                display:inline-block !important;
            }
            
            .hide-mobi {
                display:none !important;
            }
            
            .show-mobi {
                display:block !important;
            }
        }
            
        @media screen and (max-width: 340px) {
            .tbl-footer-tr {
                padding: 30px 15px !important;
            }
        }
    </style>
</head>
<body bgcolor="#e7e7e7" width="100%" style="margin: 0;" yahoo="yahoo">
<table bgcolor="#e7e7e7" cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;">
    <tr>
        <td>
            <center style="width:100%;">
            <table cellspacing="0" cellpadding="0" border="0" align="center" bgcolor="#e7e7e7" width="800" class="email-container">
                <tr>
                    <td align="left" style="padding: 25px 0 25px 0;"><img src="{$siteurl}/images/indsights-logo.png" width="172" alt="Indsights" border="0" style="margin: 0 auto; display: block; width: 172px; max-width: 100%; height: auto;"></td>
                </tr>
                <tr>
                    <td align="left">
                        <p>Hi $name!</p>
                        <p>Verify your Indsights account by verifying your email address ($email).</p>
                        <p><a href="$siteurl/verify/$verifykey" target="_blank">Verify email address</a></p>
                        <p>Button not working? Paste the following link into your browser:<br>$siteurl/verify/$verifykey</p>   
                        <p><b>The SAB Indsights Team</b></p>
                        <p style="font-size: 11px;">You're receiving this email because you recently created a new SAB INDsights profile. If this wasn't you, please ignore this email.</p>
                    </td>
                </tr>
            </table>
            </center>
        </td>
    </tr>
</table>
</body>
</html>
REGEMAIL;
                // Now send the confirmation email to the user
                $mailheader = "From: SAB INDSights <noreply@sabentrepreneurshipindsights.co.za>\r\n";
                $mailheader .= "Reply-To: noreply@sabentrepreneurshipindsights.co.za\r\n";
                $mailheader .= "MIME-Version: 1.0" . "\r\n";
                $mailheader .= "Content-type:text/html; charset=UTF-8" . "\r\n";
                mail($email, "Please verify your Indsights account", $messagebody, $mailheader);

				header("Content-type: application/json; charset=utf-8");
                http_response_code(200);
                exit(json_encode(array("success"=>$userid)));
            } else{
                header("Content-type: application/json; charset=utf-8");
                http_response_code(200);
                // No registration occurred
                if(!$userid)
                    exit(json_encode(array("errors"=>array("Bad data"))));
                else
                    exit(json_encode(array("errors"=>array($userid))));
            }
        } else{
			header("Content-type: application/json; charset=utf-8");
            http_response_code(500);
            exit(json_encode($errorarray));
        }
    } else{
        http_response_code(500);
        exit("No direct script access allowed");
    }