<?php
    session_name("INDSights");
    session_start();
    if(isset($_POST) && !empty($_POST)){
        $errorarray = array("errors"=>array());
        $valuearray = array();

        // Validate/sanitize the field data
        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
        if(!!$email && empty($email))
            array_push($errorarray["errors"], "Malformed Email address.");
        else
            array_push($valuearray, $email);

        $password = filter_input(INPUT_POST, "password", FILTER_UNSAFE_RAW);
        if(!!$password && empty($password))
            array_push($errorarray["errors"], "Malformed Password.");
        else
            array_push($valuearray, $password);

        if(empty($errorarray["errors"])){
            require_once("config/database.php");
            $logindb = new dataHandler();
            $validuser = $logindb->loginUser($valuearray);
            if(!empty($validuser) && is_array($validuser) && count($validuser) > 0){
                $_SESSION["loggedin"] = TRUE;
                $_SESSION["uid"] = $validuser["id"];
                $_SESSION["name"] = $validuser["name"];
                $_SESSION["company"] = $validuser["company"];
                $_SESSION["industry"] = $validuser["industry"];
                $_SESSION["industryname"] = $validuser["industryname"];
                unset($validuser);
                
                //header("Location: ../indsight");
                
                header("Content-type: application/json; charset=utf-8");
                http_response_code(200);
                exit(json_encode(array("success"=>TRUE)));
            } else{
				header("Content-type: application/json;");
                http_response_code(200);
                exit(json_encode(array("errors"=>array("No valid user."))));
            }
        } else{
			header("Content-type: application/json;");
            http_response_code(200);
            exit(json_encode($errorarray));
        }
    } else{
        http_response_code(500);
        exit("No direct script access allowed");
    }