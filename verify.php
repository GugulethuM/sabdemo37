<?php
    session_name("INDSights");
    session_start();
    if(isset($_GET["vkey"]) && !empty($_GET["vkey"])){
        require_once("utility/config/database.php");
        $verifydb = new dataHandler();
        $validuser = $verifydb->verifyUser($_GET["vkey"]);
        if(!!$validuser && is_numeric($validuser)){
            header("Location: /?verified=true");
            exit();
        }
    }
    header("Location: /?verified=false");