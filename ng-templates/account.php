<?php
    session_name("INDSights");
    session_start();

    require_once("../utility/config/database.php");
    $db = new dataHandler();

    $industries = $db->getIndustriesList();

    $accountdetails = $db->getAccountDetails($_SESSION["uid"]);
?>
<div class="account container-fluid">
    <div class="account__container">
        <div class="row">
            <div class="col-xs-6">
                <!-- account-->
                <div class="account__left">
                    <div class="account__image tc">
                        <img src="images/svg/SVG - No Image Icon.svg" >
                    </div>
                    <div class="account__red-line">
                        <img src="images/svg/SVG- Red Line Graphic.svg">
                    </div>
                    <div class="insights__triangles">
                        <img src="images/svg/v2/triangle-type.svg" class="img__insights">
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <!-- not a member -->
                <div class="account__right">
                   <div class="row">
                        <div class="col-xs-12">
                            <img src="images/svg/your_account.svg" />
                        </div>
                        <form name="accountform" action="utility/updateprofile.php" method="post">
                            <input type="hidden" name="userid" value="<?php echo $accountdetails["id"]; ?>" />
                            <div class="col-xs-12">
                                <input type="text" name="name" id="name" placeholder="Name" value="<?php echo $accountdetails["name"]; ?>" required />
                            </div>
                            <div class="col-xs-12">
                                <input type="text" name="company" id="company" placeholder="Company" value="<?php echo $accountdetails["company"]; ?>" required />
                            </div>
                            <div class="col-xs-12">
                                <input type="email" name="email" id="email" placeholder="Email address" value="<?php echo $accountdetails["email"]; ?>" required />
                            </div>
                            <div class="col-xs-12">
                                <input type="text" name="phone" id="phone" placeholder="Phone number" value="<?php echo $accountdetails["phone"]; ?>" required />
                            </div>
                            <div class="col-xs-12">
                                <select name="industry" id="industry">
                                    <option value="">Industry</option>
<?php
    foreach($industries as $industry){
?>
                                    <option value="<?php echo $industry["id"]; ?>"<?php if($industry["id"] == $accountdetails["industry"]) echo " selected"; ?>><?php echo $industry["industry"]; ?></option>
<?php
    }
?>
                                </select>
                            </div>
                            <!-- <div class="col-xs-12">
                                <select name="goods" id="goods">
                                    <option value="">Goods / Services</option>
                                </select>
                            </div> -->
                            <div class="col-xs-6 pt35">
                                <button type="submit" name="updatesubmit" class="btn__grey">SAVE</button>
                            </div>
                            <div class="col-xs-6 pt35">
                                <a href="./logout" class="btn__trans">logout</a>
                            </div>
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>