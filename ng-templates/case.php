<?php
    session_name("INDSights");
    session_start();

    require_once("../utility/config/database.php");
    $db = new dataHandler();
    $businesscase = $db->getBusinessCase($_SESSION["bcid"]);
?>
<div class="container-fluid" ng-init="formatFilter(3)">
    <div class="cases">
        <div class="section cases__container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <img src="images/svg/v2/business-cases.svg" class="img__cases">
                </div>
                <div class="col-md-6 col-xs-12 tr"><a href="./business-cases" class="btn__back-white">Back to Business Cases</a></div>
            </div>
            <!-- search-->
            <div class="row pg__cases">
                <div class="col-xs-12">
                    <h2><?php echo $businesscase["title"]; ?><!--<br /><small><?php //echo $businesscase["industryname"]; ?></small>--></h2>
                </div>
            </div>
            <div class="row pt30" ng-init="initContentBlocks()">
                <div class="col-md-4 col-xs-12">
                    <div class="cases__item">
                        <h2>Industry</h2>
                        <?php echo $businesscase["industry_overview"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="cases__item">
                        <h2>Product Service</h2>
                        <?php echo $businesscase["product_service"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="cases__item">
                        <h2>Initial Situation</h2>
                        <?php echo $businesscase["current_situation"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="cases__item">
                        <h2>Complication</h2>
                        <?php echo $businesscase["complication"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="cases__item">
                        <h2>Project Objective</h2>
                        <?php echo $businesscase["project_objective"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="cases__item">
                        <h2>Approach</h2>
                        <?php echo $businesscase["approach"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="cases__item">
                        <h2>Outcomes</h2>
                        <?php echo $businesscase["outcomes"]; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>