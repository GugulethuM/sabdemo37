<?php
    require_once("../utility/config/database.php");
    $db = new dataHandler();
    $industrieslist = $db->getIndustriesList();
?>
<div class="register">
    <div class="register__container">
        <form name="registrationform" action="utility/register.php" method="POST">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <!-- register-->
                    <div class="register__left">
                        <div class="row">
                            <div class="col-xs-12">
                                <img src="images/svg/v2/sign-up.svg" class="img__signup">
                            </div>
                            <div class="col-xs-12">
                                <input type="text" name="name" id="name" placeholder="Name & Surname" required />
                            </div>
                            <div class="col-xs-12">
                                <input type="email" name="email" id="email" placeholder="Email" required />
                            </div>
                            <div class="col-xs-12">
                                <input type="text" name="company" id="company" required placeholder="Company" />
                            </div>
                            <div class="col-xs-12">
                                <input type="text" name="phone" id="phone" required placeholder="Phone number" pattern="[0-9\s]{10,}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <!-- not a member -->
                    <div class="register__right">
                        <div class="row">
                            <div class="col-xs-12 pt72">
                                <!-- <input type="text" name="industry" id="industry" placeholder="Industry" required /> -->
                                <select name="industry" id="industry" required>
                                    <option value="">Industry</option>
<?php
    foreach($industrieslist as $industryitem){
?>
                                    <option value="<?php echo $industryitem["id"]; ?>"><?php echo $industryitem["industry"]; ?></option>
<?php
    }
?>
                                </select>
                            </div>
                            <div class="col-xs-12">
                                <input type="password" name="password" id="password" placeholder="Password" required />
                            </div>
                            <div class="col-xs-12">
                                <input type="password" name="password2" id="password2" placeholder="Confirm password" required />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 pt35">
                    <input type="submit" name="regsubmit" value="Submit" class="btn__grey" >
                    <button class="btn__trans" onclick="window.location='./'; return false;">Already a member Sign In</button>
                </div>
            </div>
        </form>
    </div>
</div>