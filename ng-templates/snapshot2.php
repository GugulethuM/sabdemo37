<?php
    session_name("INDSights");
    session_start();

    require_once("../utility/config/database.php");
    $db = new dataHandler();

    $industries = $db->getIndustriesList();

    if(isset($_SESSION["tempindustry"])){
        $tempindustryid = $db->getIndustryID($_SESSION["tempindustry"]);
        $filterdisplay = $_SESSION["tempindustry"];
        // unset session value
        unset($_SESSION["tempindustry"]);
        $industrysnapshot = $db->getIndustrySnapshot($tempindustryid);
    } else{
        $industrysnapshot = $db->getIndustrySnapshot($_SESSION["industry"]);
        $filterdisplay = $_SESSION["industryname"];
    }
?>
<div class="container-fluid" ng-init="formatFilter(2)">
    <div class="snapshot">
        <div class="section snapshot__container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h1 class="industrydisplay"><?php echo $filterdisplay; ?></h1>
                    <img src="images/svg/v2/snapshot-of-my-industry.svg" class="img__snapshot">
                </div>
                <div id="filterextractcontainer" class="col-md-6 col-xs-12 tr">
                
                <div class="fr z98">
                <div class="btn__lightgrey btn__lightgrey--nopad" id="snapshotfilter"><div class="btn__snapshot"><?php echo $filterdisplay; ?>&nbsp;&nbsp;<i class="btn-down"><img src="images/svg/arrow.svg"></i></div><div class="filter-groups" id="snapshot_group">
<?php
    foreach($industries as $industry){
?>
                        <div class="filter-group"><a href="./snapshot/<?php echo $industry["industry"]; ?>"><?php echo $industry["industry"]; ?></a></div>
<?php
    }
?>
                    </div></div>
                </div>
                <div class="fr z99">
                    <button id="extractreport" class="btn__grey">extract report</button>   
                </div>
                
            </div>
                
            </div>
            <div class="row pt30" ng-init="initContentBlocks()">
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Political</h2>
                        <?php echo $industrysnapshot["political"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Economical</h2>
                        <?php echo $industrysnapshot["economical"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Social</h2>
                        <?php echo $industrysnapshot["social"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Technological</h2>
                        <?php echo $industrysnapshot["technological"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Legal</h2>
                        <?php echo $industrysnapshot["legal"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Environmental</h2>
                        <?php echo $industrysnapshot["environmental"]; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>