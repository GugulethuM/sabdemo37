<?php
    session_name("INDSights");
    session_start();

    require_once("../utility/config/database.php");
    $db = new dataHandler();

    $industries = $db->getIndustriesList();

    if(isset($_SESSION["tempindustry"])){
        $tempindustryid = $db->getIndustryID($_SESSION["tempindustry"]);
        $filterdisplay = $_SESSION["tempindustry"];
        // unset session value
        unset($_SESSION["tempindustry"]);
        $industrybestpractices = $db->getIndustryBestPractice($tempindustryid);
    } else{
       $industrybestpractices = $db->getIndustryBestPractice($_SESSION["industry"]);
       $filterdisplay = $_SESSION["industryname"];
    }
?>
<div class="container-fluid" ng-init="formatFilter(5)">
    <div class="snapshot">
        <div class="section snapshot__container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <h1 class="industrydisplay"><?php echo $filterdisplay; ?></h1>
                    <img src="images/svg/v2/best-practices.svg" class="img__best">
                </div>
                <div id="filterextractcontainer" class="col-xs-12 col-md-6 tr">
                    <button class="btn__lightgrey btn__lightgrey--nopad" id="snapshotfilter"><div class="btn__snapshot"><?php echo $filterdisplay; ?>&nbsp;&nbsp;<i class="btn-down">&gt;</i></div><div class="filter-groups" id="snapshot_group">
<?php
    foreach($industries as $industry){
?>
                        <div class="filter-group"><a href="./best-practices/<?php echo $industry["industry"]; ?>"><?php echo $industry["industry"]; ?></a></div>
<?php
    }
?>
                    </div></button>
                    <button id="extractreport" class="btn__grey">extract report</button>
                </div>
            </div>
            <div class="row pt30" ng-init="initContentBlocks()">
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Strategy</h2>
                        <?php echo $industrybestpractices["strategy"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>People & Culture</h2>
                        <?php echo $industrybestpractices["people_culture"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Sales & Marketing</h2>
                        <?php echo $industrybestpractices["sales_marketing"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Operations</h2>
                        <?php echo $industrybestpractices["operations"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Technology</h2>
                        <?php echo $industrybestpractices["technology"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Enviromental</h2>
                        <?php echo $industrybestpractices["environmental"]; ?>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="snapshot__item">
                        <h2>Financial</h2>
                        <?php echo $industrybestpractices["financial"]; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>