<?php
    session_name("INDSights");
    session_start();

    require_once("../utility/config/database.php");
    $db = new dataHandler();

    $industries = $db->getBarometerIndustriesList();

    if(isset($_SESSION["tempindustry"])){
        $tempindustryid = $db->getIndustryID($_SESSION["tempindustry"]);
        $filterdisplay = $_SESSION["tempindustry"];
        // unset session value
        unset($_SESSION["tempindustry"]);
        $industrybarometer = $db->getIndustryBarometer($tempindustryid);
    } else{
        $industrybarometer = $db->getIndustryBarometer($_SESSION["industry"]);
        $filterdisplay = $_SESSION["industryname"];
    }
?>
<div class="container-fluid" ng-init="formatFilter(4)">
    <div class="barometer">
        <div class="section barometer__container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h1 class="industrydisplay"><?php echo $filterdisplay; ?></h1>
                    <img src="images/svg/v2/comp-barometer.svg" class="img__bar">
                </div>
                <div id="filterextractcontainer" class="col-md-6 col-xs-12 tr">
                
                <div class="fr z98">
                <div class="btn__lightgrey btn__lightgrey--nopad" id="snapshotfilter"><div class="btn__snapshot" ><?php echo $filterdisplay; ?>&nbsp;&nbsp;<i class="btn-down"><img src="images/svg/arrow.svg"></i></div><div class="filter-groups" id="snapshot_group">
<?php
    foreach($industries as $industry){
?>
                       <a href="./barometer/<?php echo $industry["industry"]; ?>" style="color:#ffffff;"> <div class="filter-group"><?php echo $industry["industry"]; ?></div></a>
<?php
    }
?>
                    </div></div>
                </div>
                <div class="fr z99">
                    <button id="extractreport" class="btn__grey">extract report</button>   
                </div>
                
            </div>
                
            </div>
            <div class="barometer__items" ng-init="initContentBlocks()">

                <div class="barometer__item">
<?php
    if((int)$industrybarometer["substitution_level"] === 1)
        $substitutionclass = " class=\"grey\"";
    else if((int)$industrybarometer["substitution_level"] === 2)
        $substitutionclass = " class=\"red\"";
    else
        $substitutionclass = "";
?>
                    <h2<?php echo $substitutionclass; ?>>Threat of Substitution...</h2>
                    <?php echo $industrybarometer["substitution"]; ?>
                </div>
                <div class="barometer__item">
<?php
    if((int)$industrybarometer["entry_level"] === 1)
        $entryclass = " class=\"grey\"";
    else if((int)$industrybarometer["entry_level"] === 2)
        $entryclass = " class=\"red\"";
    else
        $entryclass = "";
?>
                    <h2<?php echo $entryclass; ?>>Threat of new entry...</h2>
                    <?php echo $industrybarometer["entry"]; ?>
                </div>
                <div class="barometer__item">
<?php
    if((int)$industrybarometer["rivalry_level"] === 1)
        $rivalryclass = " class=\"grey\"";
    else if((int)$industrybarometer["rivalry_level"] === 2)
        $rivalryclass = " class=\"red\"";
    else
        $rivalryclass = "";
?>
                    <h2<?php echo $rivalryclass; ?>>COMPETITION RIVALRY</h2>
                    <?php echo $industrybarometer["rivalry"]; ?>
                </div>
                <div class="barometer__item">
<?php
    if((int)$industrybarometer["buyer_level"] === 1)
        $buyerclass = " class=\"grey\"";
    else if((int)$industrybarometer["buyer_level"] === 2)
        $buyerclass = " class=\"red\"";
    else
        $buyerclass = "";
?>
                    <h2<?php echo $buyerclass; ?>>BUYER POWER</h2>
                    <?php echo $industrybarometer["buyer"]; ?>
                </div>
                <div class="barometer__item">
<?php
    if((int)$industrybarometer["supplier_level"] === 1)
        $supplierclass = " class=\"grey\"";
    else if((int)$industrybarometer["supplier_level"] === 2)
        $supplierclass = " class=\"red\"";
    else
        $supplierclass = "";
?>
                    <h2<?php echo $supplierclass; ?>>SUPPLIER POWER</h2>
                    <?php echo $industrybarometer["supplier"]; ?>
                </div>
               

            </div>
            <div class="barometer__graph">
<?php
    // Loop through all the "levels" values and build the graph
    $barometersections = array("substitution", "entry", "rivalry", "buyer", "supplier");
    $slicearray = array();
    foreach($barometersections as $barometersection){
        array_push($slicearray, (int)$industrybarometer[$barometersection . "_level"]);
    }
    sort($slicearray, SORT_NUMERIC);
    //var_dump($slicearray);
    $lowthreat = 0;
    $mediumthreat = 0;
    $highthreat = 0;
    $threatcount = 0;
    foreach($slicearray as $slice){
        $threatcount++;
        if($slice === 1)
            $mediumthreat++;
        else if($slice === 2)
            $highthreat++;
        else
            $lowthreat++;
        if($slice === 1)
            $graphclass = " grey";
        else if($slice === 2)
            $graphclass = " red";
        else
            $graphclass = "";
?>
                    <div class="barometer__graph--slice<?php echo $graphclass; ?>"></div>
<?php
    }
?>
            </div>
            <div class="barometer__threats">
<?php
    if($lowthreat > 0){
?>
                <div class="barometer__threat--low barometer__threat" style="width: <?php echo (($lowthreat / $threatcount) * 100); ?>%;">
                    <h1>low threat</h1>
                </div>
<?php
    }
    if($mediumthreat > 0){
?>
                <div class="barometer__threat--medium barometer__threat" style="width: <?php echo (($mediumthreat / $threatcount) * 100); ?>%;">
                    <h1>medium threat</h1>
                </div>
<?php
    }
    if($highthreat > 0){
?>
                <div class="barometer__threat--high barometer__threat" style="width: <?php echo (($highthreat / $threatcount) * 100); ?>%;">
                    <h1>high threat</h1>
                </div>
<?php
    }
?>
                
            </div>
        </div>
    </div>
</div>