<?php
    session_name("INDSights");
    session_start();

    require_once("../utility/config/database.php");
    $db = new dataHandler();
    $industryindsight = $db->getIndustryIndsight($_SESSION["industry"]);
?>
<div class="insights container-fluid">
    <div class="insights__container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <!-- insights-->
                <div class="insights__left">
                    <div class="insights__image tc" style="background-image: url('images/bg/bulb.jpg'); background-position: 50% 50%; background-size: auto 100%;"></div>
                    <div class="insights__red-line">
                        <img src="images/svg/SVG- Red Line Graphic.svg">
                    </div>
                    <div class="insights__triangles">
                        <img src="images/svg/v2/triangle-type.svg" class="img__insights">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <!-- not a member -->
                <div class="insights__right">
                    <div class="insights__grey"></div>
                    
                    <div class="insights__right--inner">
                        <div class="row">
                            <div class="col-xs-12">
<?php
    if(!empty($industryindsight)){
?>
                                <span style="display: block; font-size: 1.25rem; letter-spacing: 3px; text-transform: uppercase;"><?php echo $_SESSION["industryname"]; ?> INDSIGHT</span>
                                <img src="images/svg/v2/insight-of-the-week.svg" style="margin-top: -40px; display: block;" />
<?php
    }
?>
                            </div>
                            <div class="col-xs-12">
                                <?php echo $industryindsight; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>